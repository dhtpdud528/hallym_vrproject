﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIComponentReporter : MonoBehaviour
{
    // Start is called before the first frame update
    Text text;
    void Start()
    {
        text = GetComponent<Text>();
        StartCoroutine(HUDRemove());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    IEnumerator HUDRemove()
    {
        yield return new WaitForSecondsRealtime(3f);
        while (text.color.a > 0)
        {
            yield return new WaitForSecondsRealtime(0.01f);
            text.color = new Color(255,255,255, text.color.a - Time.unscaledDeltaTime);
        }
        Destroy(gameObject);
    }
}

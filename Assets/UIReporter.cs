﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIReporter : MonoBehaviour
{
    public GameObject HUDReportComponent;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ReportState(string text)
    {
        Text itemText = Instantiate(HUDReportComponent,transform).GetComponent<Text>();
        itemText.text = text;
    }
}

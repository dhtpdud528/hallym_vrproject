﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestWaveSpawner : MonoBehaviour
{
    [SerializeField] GameObject wave;
    [SerializeField] Transform wavePoket;
    public float spreadInterval;
    public float maxVolume;
    // Start is called before the first frame update
    private void Awake()
    {
    }
    void Start()
    {
        StartCoroutine(spawnWaves());
    }

    // Update is called once per frame
    void Update()
    {

    }
    IEnumerator spawnWaves()
    {
        while (true)
        //for (int i = 0; i < 8; i++)
        //{
        //    yield return new WaitForSecondsRealtime(.1f);
        //    if (audioPeer.spectrumsFreqBand[i] <= 0)
        //        continue;
        //    SoundWave spawnedWave = Instantiate(wave, transform.position, new Quaternion()).GetComponent<SoundWave>();
        //    spawnedWave.volume = audioPeer.spectrumsFreqBand[i] * 100;
        //    spawnedWave.transform.SetParent(wavePoket);
        //}
        {
            yield return new WaitForSecondsRealtime(spreadInterval);
            SoundWave spawnedWave = Instantiate(wave, transform.position, new Quaternion()).GetComponent<SoundWave>();
            spawnedWave.volume = maxVolume;
            spawnedWave.transform.SetParent(wavePoket);
        }
    }
}

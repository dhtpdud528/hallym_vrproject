﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIInv : MonoBehaviour
{
    public GameObject button;
    public GameObject[] invs;
    private PlayerInv playerInv;
    private void OnEnable()
    {
        StartCoroutine(StartBulletTime());
    }
    private void OnDisable()
    {
        button.SetActive(true);
        StopAllCoroutines();
        Time.timeScale = 1;
        AudioSource[] audioSources = FindObjectsOfType<AudioSource>();
        for (int i = 0; i < audioSources.Length; i++)
            audioSources[i].pitch = 1;

    }
    IEnumerator StartBulletTime()
    {
        AudioSource[] audioSources = FindObjectsOfType<AudioSource>();
        while (Time.timeScale > 0.1f)
        {
            Time.timeScale -= 0.01f;
            for (int i = 0; i < audioSources.Length; i++)
                if (audioSources[i].pitch > 0.2f)
                    audioSources[i].pitch = Time.timeScale;
            yield return new WaitForSecondsRealtime(0.01f);
        }
    }
    private void Awake()
    {
        playerInv = FindObjectOfType<PlayerInv>();
    }
    private void Update()
    {
        RaycastHit hit;
        if (!Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, Mathf.Infinity, (1 << 5)))
            gameObject.SetActive(false);
    }
    public void UpdateUI()
    {
        for (int i = 0; i < invs.Length; i++)
        {
            Item item = playerInv.quickInv[i].GetComponent<Item>();
            if (invs[i].transform.childCount > 0)
            {
                if (item as ItemGun)
                    invs[i].transform.GetChild(0).GetComponent<Text>().text = ((ItemGun)item).currentBlullets + " / " + playerInv.SearchAmmo(((ItemGun)item).ammoType).GetComponent<Item>().totalNum;
                else if (item.itemName == "")
                    invs[i].transform.GetChild(0).GetComponent<Text>().text = "0";
                else
                    invs[i].transform.GetChild(0).GetComponent<Text>().text = playerInv.CountItem_inBackpack(item.itemName).ToString();
            }
            if (item as ItemMelee)
                invs[i].GetComponent<Image>().sprite = item.icon;
        }

    }
}

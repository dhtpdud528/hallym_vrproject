﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TouchScript : MonoBehaviour
{
    // Start is called before the first frame update
    public Vector3 ScreenCenter;
    void Start()
    {
        Screen.orientation = ScreenOrientation.Landscape;
        Screen.orientation = ScreenOrientation.AutoRotation;
        Screen.autorotateToPortrait = false;
        Screen.autorotateToPortraitUpsideDown = true;
        ScreenCenter = new Vector3(Camera.main.pixelWidth / 2,
            Camera.main.pixelHeight / 2);
    }

    // Update is called once per frame
    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(ScreenCenter);
        RaycastHit hit;
        if (Input.GetKeyDown(KeyCode.Mouse0) && Physics.Raycast(ray, out hit) && hit.collider.GetComponent<Button>() != null)
            hit.collider.GetComponent<Button>().onClick.Invoke();
    }
}

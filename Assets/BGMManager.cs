﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGMManager : MonoBehaviour
{
    AudioSource[] audioSources = new AudioSource[2];
    AI_Witch monster;
    PlayerState player;
    // Start is called before the first frame update
    void Start()
    {
        monster = FindObjectOfType<AI_Witch>();
        player = FindObjectOfType<PlayerState>();
        for (int i = 0; i < transform.childCount; i++)
            audioSources[i] = transform.GetChild(i).GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (monster.anim.enabled)
        {
            float distance = Vector3.Distance(monster.transform.position, player.transform.position);
            audioSources[0].volume = (distance - 2f) / 20;

            int index = 1;
            for (int i = 1; i < transform.childCount; i++)
                audioSources[i].volume = 0;
            audioSources[index].volume = 1 - audioSources[0].volume;
        }
        else
        {
            audioSources[0].volume = 1;
            audioSources[1].volume = 0;
        }
    }
}

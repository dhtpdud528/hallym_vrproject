﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDLungs : MonoBehaviour
{
    Image hud;
    PlayerState playerState;
    // Start is called before the first frame update
    void Start()
    {
        hud = GetComponent<Image>();
        playerState = FindObjectOfType<PlayerState>();
    }

    // Update is called once per frame
    void Update()
    {
        hud.color = new Color(255,255,255, (playerState.lungs-10)/10);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReflectionWave : MonoBehaviour
{
    [SerializeField] GameObject wave;
    Transform wavePoket;
    public float maxVolume;
    public void spawnReflectionWaves(float inputWaveValue)
    {
        SoundWave spawnedWave = Instantiate(wave, transform.position, new Quaternion()).GetComponent<SoundWave>();
        spawnedWave.volume = inputWaveValue < maxVolume ? inputWaveValue : maxVolume;
        spawnedWave.transform.SetParent(wavePoket);
        spawnedWave.owner = gameObject;
    }
    private void Start()
    {
        wavePoket = GameObject.Find("Waves").transform;
    }
    private void Update()
    {
        
    }
}

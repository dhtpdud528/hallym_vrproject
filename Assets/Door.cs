﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    public GameObject collider;
    public Animator anim;
    public AudioSource audioSource;
    public AudioClip[] lockSounds;
    public AudioClip[] unlockSounds;
    public AudioClip[] openSounds;
    public AudioClip[] closeSounds;
    public AudioClip[] shotedSounds;
    public AudioClip[] breakSounds;
    public bool lockDoor;
    public float maxHealth;
    private float health;
    public int id;
    public AudioClip[] unlockingSounds;

    // Start is called before the first frame update
    void Start()
    {
        if (Random.Range(0, 10) > 6)
            lockDoor = false;
        health = maxHealth;
        id = Random.Range(0, 99999);
        anim = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("MonsterBody") && !anim.GetBool("open"))
        {
            anim.SetBool("open", true);
            PlayRandomAudio(openSounds);
            GetComponent<WaveSpawner>().isVirtualSound = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("MonsterBody")||other.CompareTag("Player") && anim.GetBool("open"))
        {
            anim.SetBool("open", false);
            PlayRandomAudio(closeSounds);
            if (other.CompareTag("MonsterBody"))
                GetComponent<WaveSpawner>().isVirtualSound = true;
        }
    }
    public void PlayRandomAudio(AudioClip[] audios)
    {
        // pick & play a random footstep sound from the array,
        // excluding sound at index 0
        int n = Random.Range(1, audios.Length);
        audioSource.clip = audios[n];
        audioSource.PlayOneShot(audioSource.clip);
        // move picked sound to index 0 so it's not picked next time
        audios[n] = audios[0];
        audios[0] = audioSource.clip;
    }
    public void TakeDamage(float damage)
    {
        if (!lockDoor) return;
        health -= damage;
        if (health <= 0)
        {
            lockDoor = false;
            anim.SetBool("open", true);
            PlayRandomAudio(openSounds);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixTransform : MonoBehaviour
{
    public Transform target;
    public bool fixRotationZero;
    public bool PosLerp;
    private Vector3 OriginPos;
    private Quaternion OriginRot;
    public bool pos;
    private void Start()
    {
        if (target != null)
            OriginPos = target.localPosition;
    }
    void Update()
    {
        if (pos)
            if (PosLerp)
                transform.localPosition = Vector3.Lerp(transform.localPosition, OriginPos, 5 * Time.deltaTime);
            else
                transform.position = target.transform.position;
        if (fixRotationZero)
            transform.localRotation = new Quaternion(0, 0, 0, 1);
    }
}

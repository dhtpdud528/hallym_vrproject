﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AudioPeer : MonoBehaviour
{
    public AudioSource _audioSource;
    public float[] data = new float[256];
    public float volume;
    private Transform lister;
    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
        lister = FindObjectOfType<AudioListener>().transform;
    }

    // Update is called once per frame
    void Update()
    {
        float distanceValue = Vector3.Distance(transform.position, lister.position) > 0 ? Vector3.Distance(transform.position, lister.position) : 1;
        float volTemp = (GetRMS(0) + GetRMS(1)) * distanceValue;
        volume = volTemp > 0.0001f ? volTemp : 0;
    }
    float GetRMS(int channel)
    {
        _audioSource.GetOutputData(data, channel);
        float sum = 0;
        foreach (float f in data)
            sum += f * f;
        return Mathf.Sqrt(sum / data.Length);
    }
}

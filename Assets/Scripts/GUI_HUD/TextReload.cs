﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextReload : MonoBehaviour {
    private HUDStateInfo hudStateInfo;
    private Text textReload;
	// Use this for initialization
	void Start () {
        hudStateInfo = FindObjectOfType<HUDStateInfo>();
        textReload = GetComponent<Text>();
    }
	
	// Update is called once per frame
	void Update () {
        if (hudStateInfo.GetCurrentAmmo() <= 0)
            textReload.enabled = true;
        else
            textReload.enabled = false;

    }
}

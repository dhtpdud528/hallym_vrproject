﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDStateInfo : MonoBehaviour
{
    private Text HUDWeaponName;
    private Text HUDCurrentAmmo;
    private Text HUDLeftAmmo;
    private Text HUDLeftFirstAid;

    private Image IconLeftFirstAid;
    private Image IconBloody;

    private Image BarHealth;
    private Image BarMaxHealth;
    private Text HealthText;
    private RectTransform healthInfo;

    private PlayerInv p_inv;
    private PlayerState p_state;
    private float uiDisableTime = 0;
    private float damagedTime;
    [SerializeField]private float takenDamage;
    private Vector3 hudHealthOrigin;
    private float blueColor;

    // Use this for initialization
    void Awake()
    {
        IconBloody = GameObject.Find("Bloody").GetComponent<Image>();
        healthInfo = GameObject.Find("HealthInfo").GetComponent<RectTransform>();
        hudHealthOrigin = healthInfo.localPosition;
        HUDWeaponName = GameObject.Find("HUDWeaponName").GetComponent<Text>();
        HUDCurrentAmmo = GameObject.Find("HUDCurrentAmmo").GetComponent<Text>();
        HUDLeftAmmo = GameObject.Find("HUDLeftAmmo").GetComponent<Text>();
        HUDLeftFirstAid = GameObject.Find("HUDFirstAid").GetComponent<Text>();
        IconLeftFirstAid = GameObject.Find("IconFirstAid").GetComponent<Image>();
        BarHealth = GameObject.Find("BarHealth").GetComponent<Image>();
        BarMaxHealth = GameObject.Find("BarMaxHealth").GetComponent<Image>();
        HealthText = GameObject.Find("HealthText").GetComponent<Text>();
        p_inv = FindObjectOfType<PlayerInv>();
        p_state = FindObjectOfType<PlayerState>();
    }
    public int GetCurrentAmmo()
    {
        if(HUDCurrentAmmo.text != "")
            return System.Convert.ToInt32(HUDCurrentAmmo.text);
        return 1;
    }
    private void FixedUpdate()
    {
        BarHealth.fillAmount = (float)p_state.GetHealth() / p_state.maxHeath;
        HealthText.text = p_state.GetHealth()+ "/"+p_state.maxHeath;
        

        if (damagedTime > 0)
        {
            BarHealth.color = new Color(255, 0, 0);
            damagedTime -= Time.deltaTime;
            takenDamage /= 80;
            healthInfo.position =
                new Vector3(
                healthInfo.position.x + Random.Range(-takenDamage, takenDamage),
                healthInfo.position.y + Random.Range(-takenDamage, takenDamage),
                healthInfo.position.z + Random.Range(-takenDamage, takenDamage));
            
        }
        else if (!p_state.bloody)
            BarHealth.color = new Color(255, 255, 255);
        takenDamage /= 2;

        healthInfo.localPosition = Vector3.Lerp(healthInfo.localPosition, hudHealthOrigin, 5 * Time.deltaTime);
        if (p_state.bloody)
        {
            BarHealth.color = Color.Lerp(Color.white, Color.red, Mathf.PingPong(Time.time, 1));
            IconBloody.enabled = true;
        }
        else
            IconBloody.enabled = false;

        if (p_state.down)
            BarMaxHealth.color = new Color(255, 0, 0);
        else
            BarMaxHealth.color = new Color(0, 0, 0);
        //위에 부분 나중에 피드백받아서 고칠예정 의도적 불편함
        if (uiDisableTime < 0)
        {
            HUDWeaponName.CrossFadeAlpha(0, 1f, true);
            HUDCurrentAmmo.CrossFadeAlpha(0, 1f, true);
            HUDLeftAmmo.CrossFadeAlpha(0, 1f, true);
            HUDLeftFirstAid.CrossFadeAlpha(0, 1f, true);
            IconLeftFirstAid.CrossFadeAlpha(0, 1f, true);
        }
        else
        {
            uiDisableTime -= Time.deltaTime;
            HUDWeaponName.CrossFadeAlpha(100, 0f, true);
            HUDCurrentAmmo.CrossFadeAlpha(100, 0f, true);
            HUDLeftAmmo.CrossFadeAlpha(100, 0f, true);
            HUDLeftFirstAid.CrossFadeAlpha(100, 0f, true);
            IconLeftFirstAid.CrossFadeAlpha(100, 0f, true);
        }
        if (HUDCurrentAmmo.text == "0")
            HUDCurrentAmmo.color = new Color(255, 0, 0);
        else
            HUDCurrentAmmo.color = new Color(255, 255, 255);

        if (HUDLeftAmmo.text == "0")
            HUDLeftAmmo.color = new Color(255, 0, 0);
        else
            HUDLeftAmmo.color = new Color(255, 255, 255);

        if (HUDLeftFirstAid.text == "0")
            HUDLeftFirstAid.color = new Color(255, 0, 0);
        else
            HUDLeftFirstAid.color = new Color(255, 255, 255);

    }
    // Update is called once per frame
    public void UpdateHUDInfo()
    {
        uiDisableTime = 0;
        if (p_inv != null)
        {
            HUDLeftFirstAid.text = p_inv.CountItem_inBackpack("붕대").ToString();
            if (p_inv.GetCurrentItemInfo().itemName == "")
            {
                HUDWeaponName.text = "비무장";
                HUDCurrentAmmo.text = "";
                HUDLeftAmmo.text = "";
            }
            else if (p_inv.GetCurrentItemInfo() is ItemGun)
            {
                HUDWeaponName.text = p_inv.GetCurrentItemInfo().itemName;
                HUDCurrentAmmo.text = ((ItemGun)p_inv.GetCurrentItemInfo()).currentBlullets.ToString();
                GameObject ammo = p_inv.SearchAmmo(((ItemGun)p_inv.GetCurrentItemInfo()).ammoType);
                if (ammo != null)
                    HUDLeftAmmo.text = ammo.GetComponent<ItemAmmo>().totalNum.ToString();
                else
                    HUDLeftAmmo.text = "0";


            }
            else
            {
                int count = p_inv.CountItem_inBackpack(p_inv.GetCurrentItemInfo().itemName);
                HUDWeaponName.text = p_inv.GetCurrentItemInfo().itemName;
                HUDCurrentAmmo.text = count.ToString();
                HUDLeftAmmo.text = "";
            }
        }
    }
    public void TakeDamage_HUD(float damage)
    {
        damagedTime = 0.2f;
        takenDamage += damage / 4;
        UpdateHUDInfo();
    }
    public void activeHUD()
    {
        HUDWeaponName.CrossFadeAlpha(100, 0f, true);
        HUDCurrentAmmo.CrossFadeAlpha(100, 0f, true);
        HUDLeftAmmo.CrossFadeAlpha(100, 0f, true);
        HUDLeftFirstAid.CrossFadeAlpha(100, 0f, true);
        IconLeftFirstAid.CrossFadeAlpha(100, 0f, true);
    }
}

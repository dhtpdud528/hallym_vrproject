﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class HUDInvInfo : MonoBehaviour
{
    private Text[] quickSlot = new Text[5];
    private PlayerInv playerInv;
    private float uiDisableTime = 0;
    // Use this for initialization
    void Awake()
    {
        playerInv = FindObjectOfType<PlayerInv>();
        for (int i = 0; i < 5; i++)
            quickSlot[i] = transform.GetChild(i).GetComponent<Text>();
    }
    private void FixedUpdate()
    {
        if (uiDisableTime < 0)
            for (int i = 0; i < 5; i++)
                quickSlot[i].CrossFadeAlpha(0, 1f, true);
        else
        {
            uiDisableTime -= Time.deltaTime;
            for (int i = 0; i < 5; i++)
                quickSlot[i].CrossFadeAlpha(100, 0f, true);
        }
    }

    internal void UpdateHUDInfo()
    {
        uiDisableTime = 0;
        for (int i = 0; i < 5; i++)
        {
            if (playerInv.quickInv[i].GetComponent<Item>().itemName != "")
                quickSlot[i].text = i + 1 + ". " + playerInv.quickInv[i].GetComponent<Item>().itemName;
            else
            {
                switch (i)
                {
                    case 0:
                        quickSlot[i].text = i + 1 + ". " + "Gun";
                        break;
                    case 1:
                        quickSlot[i].text = i + 1 + ". " + "Gun";
                        break;
                    case 2:
                        quickSlot[i].text = i + 1 + ". " + "Melee";
                        break;
                    case 3:
                        quickSlot[i].text = i + 1 + ". " + "Granade";
                        break;
                    case 4:
                        quickSlot[i].text = i + 1 + ". " + "FirstAidKit";
                        break;
                }
            }
            if (playerInv.quickInv[playerInv.nextIndex].GetComponent<Item>() == playerInv.quickInv[i].GetComponent<Item>())
            {
                quickSlot[i].fontSize = 20;
                quickSlot[i].color = new Color(255, 255, 255, 255);
            }
            else
            {
                quickSlot[i].fontSize = 15;
                quickSlot[i].color = new Color(255, 255, 255, 0.5f);
            }
        }
    }
    public void activeHUD()
    {
        for (int i = 0; i < 5; i++)
            quickSlot[i].CrossFadeAlpha(100, 0f, true);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDProgressBar : MonoBehaviour {

    public Image imageProgressBar;

    // Use this for initialization
    void Start () {
        imageProgressBar = GetComponent<Image>();
        imageProgressBar.fillAmount = 0;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}

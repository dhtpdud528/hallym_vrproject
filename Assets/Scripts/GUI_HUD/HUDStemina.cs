﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDStemina : MonoBehaviour
{
    private Text hud;
    private Image hudStemina;
    private Image[] barMaxStemina = new Image[2];
    private PlayerState p_state;

    // Use this for initialization
    void Start()
    {
        hud = GetComponent<Text>();
        p_state = FindObjectOfType<PlayerState>();
        hudStemina = GameObject.Find("BarStemina").GetComponent<Image>();
        barMaxStemina[0] = GameObject.Find("BarMaxStemina1").GetComponent<Image>();
        barMaxStemina[1] = GameObject.Find("BarMaxStemina2").GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        if (p_state.stemina >= 1)
        {
            hudStemina.transform.localScale = new Vector3(p_state.stemina / p_state.maxStemina, hudStemina.transform.localScale.y, hudStemina.transform.localScale.z);
            hudStemina.color = Color.Lerp(Color.white, Color.red, p_state.stemina / p_state.maxStemina);
            hudStemina.transform.GetChild(0).GetComponent<Image>().color = Color.Lerp(Color.white, Color.red, p_state.stemina / p_state.maxStemina);
        }
        else
        {
            hudStemina.color = new Color(255, 255, 255, 0);
            hudStemina.transform.GetChild(0).GetComponent<Image>().color = new Color(255, 255, 255, 0);
        }

        barMaxStemina[0].color = Color.Lerp(new Color(255, 255, 255, 0), Color.red, p_state.stemina / p_state.maxStemina);
        barMaxStemina[1].color = Color.Lerp(new Color(255, 255, 255, 0), Color.red, p_state.stemina / p_state.maxStemina);
        //hud.text = p_state.stemina.ToString() + " / " + p_state.relax.ToString();

    }
}

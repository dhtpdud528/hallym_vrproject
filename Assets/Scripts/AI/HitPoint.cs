﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitPoint : MonoBehaviour {
    public AI_Monster state;
    public enum HitParts {Head,Legs,Others };
    public HitParts HitPart;

    public void giveDamage(float damage)
    {
            state.takeDamage(damage, HitPart);//체력이 0보다 작아진다면 고통애니메이션을 재생한다. <<이부분은 ai_8feet에서 구현할것
    }
}

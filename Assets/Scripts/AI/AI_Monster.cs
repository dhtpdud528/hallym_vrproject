﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(NavMeshAgent))]
public class AI_Monster : MonoBehaviour
{
    public float damage;
    public AudioSource m_HeadAudioSource;
    public int id;
    protected GameObject allOfPlayer;
    public Animator anim;
    public NavMeshAgent agent;
    public PlayerState player;
    public float maxHealth;
    public float health;

    [SerializeField] protected AudioClip[] stepsSounds;
    public AudioClip[] headShotSounds;
    public AudioSource m_AudioSource;

    private float m_StepCycle;
    private float m_NextStep;
    [SerializeField] private float m_StepInterval;
    // Start is called before the first frame update
    public void Awake()
    {
        m_StepCycle = 0f;
        m_NextStep = 0.5f;
    }
    public void Start()
    {
        id = Random.Range(0, 99999);
        allOfPlayer = GameObject.Find("AllOfPlayer");
        agent = GetComponent<NavMeshAgent>();
        player = FindObjectOfType<PlayerState>();
        health = maxHealth;
        anim = GetComponent<Animator>();
        m_AudioSource = GetComponent<AudioSource>();
    }
    public void FixedUpdate()
    {
        ProgressStepCycle();
    }
    private void ProgressStepCycle()
    {

        if (agent.velocity.sqrMagnitude > 0)
        {
            float vel = agent.velocity.magnitude;
            if (vel > 5)
                vel = 5;
            m_StepCycle += (vel) * Time.fixedDeltaTime;
        }

        if (!(m_StepCycle > m_NextStep))
            return;

        m_NextStep = m_StepCycle + m_StepInterval;

        PlayFootStepAudio();
    }
    private void PlayFootStepAudio()
    {
        // pick & play a random footstep sound from the array,
        // excluding sound at index 0
        int n = Random.Range(1, stepsSounds.Length);
        m_AudioSource.clip = stepsSounds[n];
        m_AudioSource.Play();
        // move picked sound to index 0 so it's not picked next time
        stepsSounds[n] = stepsSounds[0];
        stepsSounds[0] = m_AudioSource.clip;
    }

    public virtual void takeDamage(float damage, HitPoint.HitParts hitPart)
    {
        if (health <= 0)
            m_AudioSource.Stop();
        player.weaponCon.PlayHitMark();
        if (hitPart == HitPoint.HitParts.Head)
        {
            PlayRandomAudio_Head(headShotSounds);
            health -= damage * 2;
        }
        else
            health -= damage;
    }
    protected void PlayRandomAudio(AudioClip[] audios)
    {
        // pick & play a random footstep sound from the array,
        // excluding sound at index 0
        int n = Random.Range(1, audios.Length);
        m_AudioSource.clip = audios[n];
        m_AudioSource.PlayOneShot(m_AudioSource.clip);
        // move picked sound to index 0 so it's not picked next time
        audios[n] = audios[0];
        audios[0] = m_AudioSource.clip;
    }
    public void PlayRandomAudio_Head(AudioClip[] audios)
    {
        // pick & play a random footstep sound from the array,
        // excluding sound at index 0
        int n = Random.Range(1, audios.Length);
        m_HeadAudioSource.clip = audios[n];
        m_HeadAudioSource.PlayOneShot(m_HeadAudioSource.clip);
        // move picked sound to index 0 so it's not picked next time
        audios[n] = audios[0];
        audios[0] = m_HeadAudioSource.clip;
    }
}

﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class AI_Witch : AI_Monster
{

    public int maxHideStack;
    public int currentHideStack;

    public float maxfaintStack;
    private float currentfaintStack;
    public float maxfaintTime;
    private float faintTime;

    public int maxRunStack = 2;
    public int currentRunStack = 0;
    public int painStack = 2;

    [SerializeField] private float patrolSpeed;
    [SerializeField] private float rushSpeed;
    [SerializeField] private float runSpeed;
    [SerializeField] private float painingSpeed;
    
    public float rushRange;

    public enum STATE { Run, Rush, Patrol, Alert };
    public STATE ai_state;
    //private Collider coverOBJ;
    public GameObject target;
    [SerializeField] private Vector3 runWayPos;

    public AudioClip[] crySounds;
    public AudioClip[] rageSounds;
    public AudioClip[] runSounds;
    public AudioClip[] painSounds;
    public AudioClip[] screamSounds;
    public AudioClip[] alertSounds;
    public AudioClip[] breathSounds;

    private AnimatorStateInfo stateInfo;
    private AnimatorTransitionInfo transInfo;

    private float direction = 0f;
    private float charAngle = 0f;
    private int slideTick = 0;
    private float hideTime = 0;
    public GameObject Head;
    private int screamTick = 0;

    [SerializeField] private GameObject dead;
    //private float reSpawnTime;
    //public bool beSeen;
    public bool isAttacking;
    public float attackRange;
    private bool beSeen;
    private Vector3 patrolDestynation;
    public float MaxNotice;
    public float notice;
    

    // Use this for initialization
    public void Start()
    {
        base.Start();
        ZombieAttackState[] attackList = anim.GetBehaviours<ZombieAttackState>();
        foreach (ZombieAttackState i in attackList)
            i.zombie = GetComponent<AI_Witch>();
        currentfaintStack = 0;
        currentRunStack = maxRunStack;
        StartCoroutine(PlayLivingSound());
        //ai_state = STATE.Stalk;
        if (ai_state == STATE.Patrol)
            StartCoroutine(StartPatrol());
    }


    // Update is called once per frame
    public void Update()
    {
        if(notice > MaxNotice)
            notice = MaxNotice;
        if (!anim.enabled)
            return;
        if (ai_state == STATE.Alert)
        {
            notice -= 10*Time.deltaTime;
            if (notice <= 0)
            {
                anim.CrossFadeInFixedTime("Crying_Loop", 0.1f);
                ai_state = STATE.Patrol;
            }
        }
        
        float rootSpeed = 0;
        stateInfo = anim.GetCurrentAnimatorStateInfo(0);
        transInfo = anim.GetAnimatorTransitionInfo(0);
        if (!anim.enabled)
        {
            faintTime += Time.deltaTime;
            if (faintTime > maxfaintTime)
            {
                anim.enabled = true;
                agent.enabled = true;
                currentfaintStack = 0;
                anim.CrossFadeInFixedTime("StandingUp", 0f);
                agent.speed = 0;
                agent.acceleration = 0;
                faintTime = 0;
            }
        }
        else if (!isFaintAway())
        {

            if (ai_state == STATE.Run)
            {
                anim.SetFloat("RunAniSpeed", 1.2f);
                target = null;
                agent.speed = runSpeed;
                agent.acceleration = runSpeed * 10;
                if (runWayPos == Vector3.zero)
                    FindRunWay();
                else
                    agent.SetDestination(runWayPos);
                if (!beSeen)
                {
                    hideTime += Time.deltaTime;
                    if (hideTime > UnityEngine.Random.Range(5, 8))
                    {
                        currentRunStack--;
                        if (isCrawlling())
                            anim.CrossFadeInFixedTime("StandUP", 0.1f);
                        maxHideStack++;
                        currentHideStack = maxHideStack;
                        ai_state = STATE.Alert;
                        StartCoroutine(StartPatrol());
                        hideTime = 0;

                    }
                }
                else if (Vector3.Distance(runWayPos, transform.position) < 4)
                {
                    FindRunWay();
                    agent.SetDestination(runWayPos);
                }

            }
            else if (ai_state == STATE.Rush)
            {
                runWayPos = Vector3.zero;

                //coverOBJ = null;
                slideTick = 0;
                target = player.gameObject;
                agent.speed = rushSpeed;
                agent.acceleration = rushSpeed*2;
                anim.SetFloat("RunAniSpeed", 1f);
                /*if (Vector3.Distance(target.transform.position, transform.position) < 4f)
                {
                    rootSpeed = 10;
                    agent.acceleration = rushSpeed * 3;
                }*/
                float dis = Vector3.Distance(target.transform.position, transform.position);
                if (!isPaining() && !isScreaming() && !player.isPain && dis < 1.5f)
                {

                    /*if (Random.Range(0, 3) == 1)
                        player.TakeDamage(0, PlayerState.m_deburf.Bloody);
                    */


                    if (agent.velocity.magnitude > 9)
                    {
                        screamTick++;
                        agent.velocity *= 0;
                        player.GetComponent<playerRigidbody>().addAcceleration(transform.up, 10);
                        player.GetComponent<playerRigidbody>().addAcceleration(transform.forward, 20);
                        player.TakeDamage(15);
                        if (Random.Range(0, 3) == 1)
                            player.TakeDamage(0, PlayerState.m_deburf.Confusion);
                        if (Random.Range(0, 3) == 1)
                            player.TakeDamage(0, PlayerState.m_deburf.Bloody);
                        //anim.CrossFadeInFixedTime("Scream", 0.1f);
                    }
                    else if (!isPaining())
                    {
                        if (isCrawlling())
                            anim.CrossFadeInFixedTime("Idle", 0.1f);
                        DoAttack();
                    }
                }
            }
            else if (ai_state == STATE.Patrol)
            {
                anim.SetFloat("RunAniSpeed", 0.8f);
                agent.speed = patrolSpeed;
                agent.acceleration = runSpeed*10;
            }
            else if (ai_state == STATE.Alert)
            {
                anim.SetFloat("RunAniSpeed", 1.2f);
                float vel = Vector3.Distance(agent.destination, transform.position);
                if (vel > 7)
                    vel = 7;
                agent.speed = vel;
                agent.acceleration = runSpeed * 10;
            }
            if (isPaining())
            {
                agent.speed = painingSpeed;
                agent.acceleration = painingSpeed;
                if (agent.velocity.magnitude > painingSpeed)
                    agent.velocity /= 1.1f;
            }
            if (isScreaming())
                agent.velocity *= 0;
            else
                screamTick = 0;

            Vector3 lookrotation = agent.steeringTarget - transform.position;
            if (IsInPivot())
                rootSpeed = 10;
            if (lookrotation != Vector3.zero)
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(lookrotation), rootSpeed * Time.deltaTime);
            if (!isScreaming() && agent.isOnNavMesh && target != null)
                agent.SetDestination(target.transform.position);



        }
    }

    IEnumerator StartPatrol()
    {
        anim.SetFloat("RunAniSpeed", 0.5f);
        anim.CrossFadeInFixedTime("Crying_Loop", 0.1f);
        while (true)
        {
            if (!(ai_state == STATE.Patrol))
            {
                yield return new WaitForSeconds(0.1f);
                continue;
            }
            agent.SetDestination(transform.position);
            yield return new WaitForSeconds(Random.Range(3, 4));
            Vector3 pos;
            RaycastHit hit;

            do
            {
                pos = transform.position;
                pos.x += Random.Range(-30, 30);
                pos.z += Random.Range(-30, 30);
                if (Physics.Raycast(new Vector3(pos.x, pos.y, pos.z), Vector3.down, out hit, Mathf.Infinity, (1 << 14)))
                    pos.y = 0.1f;
                else
                    continue;
            }
            while (pos.y >= 9999);
            agent.SetDestination(pos);
            Debug.Log(agent.destination);
            float patrolTime = 0;
            float stopTime = 0;
            while(patrolTime < Random.Range(20, 30))
            {
                patrolTime += Time.deltaTime;
                if(agent.velocity.magnitude < 0.5f)
                    stopTime += Time.deltaTime;
                if (stopTime > 2)
                    break;
                yield return new WaitForSeconds(0.05f);
            }
        }
    }

    private void ReSpawn()
    {
        Vector3 pos;
        RaycastHit hit;
        do
        {
            pos = player.transform.position;
            pos.x += Random.Range(-100, 100);
            pos.z += Random.Range(-100, 100);
            Debug.DrawRay(new Vector3(pos.x, pos.y + 5, pos.z), Vector3.down, Color.red, Time.deltaTime * 120);
        }
        while (Vector3.Distance(pos, player.transform.position) < 50 && !Physics.Raycast(new Vector3(pos.x, pos.y + 300, pos.z), Vector3.down, out hit, Mathf.Infinity, (1 << 14)));
        if (Physics.Raycast(new Vector3(pos.x, pos.y + 300, pos.z), Vector3.down, out hit, Mathf.Infinity, (1 << 14)))
            pos.y = hit.point.y;
        transform.position = pos;
    }
    public void FixedUpdate()
    {
        base.FixedUpdate();
        if (health < 0) return;
        //다른 프젝
        charAngle = 0f;
        direction = 0f;

        // Translate controls stick coordinates into world/cam/character space
        if (target != null)
            StickToWorldspace(this.transform, ref direction, ref charAngle, IsInPivot());
        anim.SetFloat("Speed", agent.velocity.magnitude / 5, 0.05f, Time.deltaTime);
        anim.SetFloat("Direction", direction, 0.25f, Time.deltaTime);

        if (agent.velocity.magnitude > LocomotionThreshold)    // Dead zone
            if (!IsInPivot())
                anim.SetFloat("Angle", charAngle);
        if (agent.velocity.magnitude < LocomotionThreshold)    // Dead zone
        {
            anim.SetFloat("Direction", 0f);
            anim.SetFloat("Angle", 0f);
        }
    }
    public void StickToWorldspace(Transform root, ref float directionOut, ref float angleOut, bool isPivoting)
    {
        Vector3 rootDirection = root.forward;

        Vector3 stickDirection = new Vector3(target.transform.position.x, 0, target.transform.position.z) - new Vector3(transform.position.x, 0, transform.position.z);


        // Convert joystick input in Worldspace coordinates
        Vector3 moveDirection = stickDirection;
        Vector3 axisSign = Vector3.Cross(moveDirection, rootDirection);



        float angleRootToMove = Vector3.Angle(rootDirection, moveDirection) * (axisSign.y >= 0 ? -1f : 1f);
        if (!isPivoting)
        {
            angleOut = angleRootToMove;
        }
        angleRootToMove /= 180f;

        directionOut = angleRootToMove * 1.5f;
    }
    /*
    IEnumerator FindClosestCover()
    {
        int layerMask = 1 << 10;
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, Mathf.Infinity, layerMask);

        float mDist = float.MaxValue;
        Collider closest = null;

        for (int i = 0; i < hitColliders.Length; i++)
        {
            if (hitColliders[i].gameObject != gameObject)
            {
                if (coverOBJ != null && hitColliders[i] == coverOBJ)
                    i++;
                if (i < hitColliders.Length)
                {
                    float tDist = Vector3.Distance(hitColliders[i].transform.position, transform.position);
                    if (tDist < mDist)
                    {
                        mDist = tDist;
                        closest = hitColliders[i];
                    }
                }
            }
        }
        coverOBJ = closest;
        yield return new WaitForSeconds(.1f);
    }*/
    private void FindRunWay()
    {
        Vector3 pos;
        RaycastHit hit;

        do
        {
            pos = player.transform.position;
            pos.x += Random.Range(-50, 50);
            pos.z += Random.Range(-50, 50);
            if (Physics.Raycast(new Vector3(pos.x, 9999, pos.z), Vector3.down, out hit, Mathf.Infinity, (1 << 14)))
                pos.y = 0.1f;
            else
                continue;
        }
        while (Vector3.Distance(pos, player.transform.position) < 30 || pos.y > 0.1f);
        runWayPos = pos;
    }
    public override void takeDamage(float damage, HitPoint.HitParts hitPart)
    {
        /*if (hitPart == HitPoint.HitParts.Head)
        {
            currentfaintStack += damage;
            health -= damage * 2;
        }
        else
        {
            health -= damage;
        }
        if (currentfaintStack > maxfaintStack)
        {
            anim.enabled = false;
            agent.enabled = false;
            return;
        }*/
        //ai.velocity /= damage/1.5f;
        base.takeDamage(damage, hitPart);
        if(ai_state == STATE.Patrol || ai_state == STATE.Alert)
        {
            m_HeadAudioSource.Stop();
            anim.Play("UpperBody.New State");
            anim.CrossFadeInFixedTime("Scream", 0.1f);
            PlayRandomAudio_Head(screamSounds);
            ai_state = AI_Witch.STATE.Rush;
        }
        if (anim.enabled && health <= 0)
        {
            if (currentRunStack <= 0)
            {
                StartCoroutine(BulletTimeEvent());
                Item KEY = Instantiate(FindObjectOfType<ItemList>().FindItem("마스터 키"),Head.transform.position,new Quaternion()).GetComponent<Item>();
                KEY.DropItem();
                m_HeadAudioSource.Stop();
                anim.enabled = false;
                agent.enabled = false;
                return;
            }
            PlayRandomAudio_Head(painSounds);
            if (!isDownPaining())
            {
                if (isStand())
                {
                    if (hitPart == HitPoint.HitParts.Head)
                    {
                        switch (UnityEngine.Random.Range(1, 3))
                        {
                            case 1:
                                anim.CrossFadeInFixedTime("UpperBody.PainHead", 0.1f);
                                break;
                            case 2:
                                anim.CrossFadeInFixedTime("UpperBody.PainHead2", 0.1f);
                                break;
                        }
                    }

                    else if (hitPart == HitPoint.HitParts.Others)
                        switch (UnityEngine.Random.Range(1, 3))
                        {
                            case 1:
                                anim.CrossFadeInFixedTime("UpperBody.PainBody", 0.1f);
                                break;
                            case 2:
                                anim.CrossFadeInFixedTime("UpperBody.PainBody2", 0.1f);
                                break;
                        }
                    else if (hitPart == HitPoint.HitParts.Legs)
                        switch (UnityEngine.Random.Range(1, 4))
                        {
                            case 1:
                                anim.CrossFadeInFixedTime("DownPain1", 0.1f);
                                anim.CrossFadeInFixedTime("DownPain.DownPain1", 0.1f);
                                break;
                            case 2:
                                anim.CrossFadeInFixedTime("DownPain2", 0.1f);
                                anim.CrossFadeInFixedTime("DownPain.DownPain2", 0.1f);
                                break;
                            case 3:
                                anim.CrossFadeInFixedTime("DownPain3", 0.1f);
                                anim.CrossFadeInFixedTime("DownPain.DownPain3", 0.1f);
                                break;
                        }
                }
                else
                    anim.CrossFadeInFixedTime("Pain", 0.1f);
            }
        }
    }
    IEnumerator BulletTimeEvent()
    {
        Time.timeScale = .05f;

        AudioSource[] audioSources = FindObjectsOfType<AudioSource>();

        for (int i = 0; i < audioSources.Length; i++)
            if (audioSources[i].pitch > 0.2f)
                audioSources[i].pitch = Time.timeScale;

        yield return new WaitForSecondsRealtime(5);

        while (Time.timeScale < 1)
        {
            Time.timeScale += .01f;
            yield return new WaitForSecondsRealtime(0.01f);
            for (int i = 0; i < audioSources.Length; i++)
                audioSources[i].pitch = Time.timeScale;
        }

        Time.timeScale = 1f;
        for (int i = 0; i < audioSources.Length; i++)
            audioSources[i].pitch = 1;
        StopAllCoroutines();
    }

    public bool isStand()
    {
        return !(stateInfo.IsName("Crawl.Crawlling") || stateInfo.IsName("Crawl.Pain"));
    }
    public float LocomotionThreshold { get { return 0.2f; } }
    public bool IsInPivot()
    {
        return stateInfo.IsName("loco.LocomotionPivotL") ||
            stateInfo.IsName("loco.LocomotionPivotR") ||
            transInfo.IsName("loco.Locomotion -> loco.LocomotionPivotL") ||
            transInfo.IsName("loco.Locomotion -> loco.LocomotionPivotR") ||
            stateInfo.IsName("Stand.slide");
    }
    public bool isPaining()
    {
        return anim.GetCurrentAnimatorStateInfo(2).IsName("DownPain1") ||
            anim.GetCurrentAnimatorStateInfo(2).IsName("DownPain2") ||
            anim.GetCurrentAnimatorStateInfo(2).IsName("DownPain3") ||
            anim.GetCurrentAnimatorStateInfo(1).IsName("PainBody") ||
            anim.GetCurrentAnimatorStateInfo(1).IsName("PainHead") ||
            anim.GetCurrentAnimatorStateInfo(1).IsName("PainBody2") ||
            anim.GetCurrentAnimatorStateInfo(1).IsName("PainHead2") ||
            anim.GetCurrentAnimatorStateInfo(0).IsName("Pain");
    }
    public bool isScreaming()
    {
        return anim.GetCurrentAnimatorStateInfo(0).IsName("Scream");
    }
    public bool isCrawlling()
    {
        return anim.GetCurrentAnimatorStateInfo(0).IsName("Crawlling");
    }
    public bool isDownPaining()
    {
        return anim.GetCurrentAnimatorStateInfo(2).IsName("DownPain1") ||
            anim.GetCurrentAnimatorStateInfo(2).IsName("DownPain2") ||
            anim.GetCurrentAnimatorStateInfo(2).IsName("DownPain3");
    }
    public bool isFaintAway()
    {
        return anim.GetCurrentAnimatorStateInfo(0).IsName("StandingUp");
    }
    public bool isCrying()
    {
        return anim.GetCurrentAnimatorStateInfo(1).IsName("Crying_Loop");
    }
    void OnAnimatorIK(int layerIndex)
    {
        if (!isPaining() && !isFaintAway() && target != null)
        {

            anim.SetLookAtWeight(0.3f);
            if (target.GetComponent<PlayerState>() == null)
                anim.SetLookAtPosition(target.transform.position);
            else
                anim.SetLookAtPosition(target.transform.GetChild(0).position);
        }
    }
    public void Spawn()
    {
        Vector3 spawnPos = player.gameObject.transform.position;
        spawnPos.x += UnityEngine.Random.Range(-40f, 40f);
        spawnPos.z += UnityEngine.Random.Range(-40f, 40f);
        while (Vector3.Distance(spawnPos, player.gameObject.transform.position) < 20)
        {
            spawnPos.x += UnityEngine.Random.Range(-40f, 40f);
            spawnPos.z += UnityEngine.Random.Range(-40f, 40f);

            new WaitForSeconds(2f);
        }
        //maxRunStack++;
        //currentRunStack = maxRunStack;
        transform.position = spawnPos;
    }
    private void DoAttack()
    {
        if (isAttacking)
            return;
        int rnd = Random.Range(1, 7);
        anim.CrossFadeInFixedTime("UpperBody.Attack" + rnd, 0.1f);
        agent.velocity /= 2;
    }
    IEnumerator PlayLivingSound()
    {
        while (true)
            if (!m_HeadAudioSource.isPlaying&&anim.enabled)
                switch (ai_state)
                {
                    case STATE.Patrol:
                        PlayRandomAudio_Head(crySounds);
                        yield return new WaitForSeconds(Random.Range(8, 12));
                        break;
                    case STATE.Run:
                        PlayRandomAudio_Head(runSounds);
                        yield return new WaitForSeconds(0.01f);
                        break;
                    case STATE.Rush:
                        if (isAttacking)
                            PlayRandomAudio_Head(rageSounds);
                        yield return new WaitForSeconds(Random.Range(3, 5));
                        break;
                    case STATE.Alert:
                        if(agent.velocity.magnitude < 2)
                            PlayRandomAudio_Head(breathSounds);
                        yield return new WaitForSeconds(Random.Range(6, 8));
                        break;
                }
            else
                yield return new WaitForSeconds(0.1f);
    }
}

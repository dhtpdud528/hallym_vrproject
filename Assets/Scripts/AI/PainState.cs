﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PainState : StateMachineBehaviour
{

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.GetComponent<AI_Witch>().painStack--;
        if (animator.GetComponent<AI_Witch>().painStack <= 0)
        {
            animator.GetComponent<AI_Witch>().painStack = 2;
            animator.GetComponent<AI_Witch>().health = animator.GetComponent<AI_Witch>().maxHealth;
            animator.GetComponent<AI_Witch>().currentHideStack--;

            if (animator.GetComponent<AI_Witch>().currentHideStack <= 0)
                animator.GetComponent<AI_Witch>().ai_state = AI_Witch.STATE.Run;
        }

    }
    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (animator.GetComponent<AI_Witch>().painStack > 0)
        {
            animator.GetComponent<AI_Witch>().health = animator.GetComponent<AI_Witch>().maxHealth;
            if (!animator.GetComponent<AI_Witch>().isPaining())
                animator.GetComponent<AI_Witch>().currentHideStack--;

            if (animator.GetComponent<AI_Witch>().currentHideStack <= 0)
                animator.GetComponent<AI_Witch>().ai_state = AI_Witch.STATE.Run;
        }
        else
            animator.GetComponent<AI_Witch>().painStack = 2;
    }

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}

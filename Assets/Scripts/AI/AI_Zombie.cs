using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public enum ZombieType
{
    Captin,
    Normal,
    NonArmored
}

public class AI_Zombie : AI_Monster
{
    public ZombieType zombieType;
    [SerializeField] private GameObject armor;
    [SerializeField] private GameObject helmet;
    [SerializeField] private GameObject skirt;
    public Vector3 destinationPos;
    public float checkRate = 0.4f;
    public float lookRadius = 10f;

    private Transform target;
    private float charAngle;
    private float direction = 0f;
    private AnimatorStateInfo stateInfo;
    private AnimatorTransitionInfo transInfo;
    private float wanderRange = 10f;
    private NavMeshHit navHit;
    private Vector3 wanderTarget;
    private Transform t;
    private float nextCheck = 0f;
    private GameObject whoIsTheTarget;
    private float timer = 0f;
    private float time = 7f;
    [SerializeField] private Material[] materials;
    private ItemList packtory;
    public bool isAttacking;
    public float attackRange;
    public bool isKnockBacking;

    public void Awake()
    {
        base.Awake();
        packtory = FindObjectOfType<ItemList>();
        t = GetComponent<Transform>();
        whoIsTheTarget = GameObject.FindGameObjectWithTag("Player");
    }
    // Use this for initialization
    public void Start()
    {
        base.Start();
        //anim.GetBehaviour<ZombieAttackState>().zombie = GetComponent<AI_Zombie>();
        if (transform.GetChild(0).name == "Modeling")
        {
            transform.GetChild(0).GetChild(0).GetComponent<SkinnedMeshRenderer>().material = materials[Random.Range(0, materials.Length)];
            transform.GetChild(0).GetChild(2).GetComponent<SkinnedMeshRenderer>().material = materials[Random.Range(0, materials.Length)];
        }
        else
            transform.GetChild(0).GetComponent<SkinnedMeshRenderer>().material = materials[Random.Range(0, materials.Length)];
        if (armor != null)
            if (zombieType == ZombieType.NonArmored)
                armor.SetActive(false);
            else if (Random.Range(0, 5) == 0)
                armor.SetActive(true);
            else
                armor.SetActive(false);
        if (helmet != null)
            if (Random.Range(0, 5) == 0)
                helmet.SetActive(true);
            else
                helmet.SetActive(false);
        float tall = Random.Range(-0.05f, 0.1f);
        transform.localScale = new Vector3(transform.localScale.x + tall, transform.localScale.y + tall, transform.localScale.z + tall);

    }
    // Update is called once per frame
    public void Update()
    {
        stateInfo = anim.GetCurrentAnimatorStateInfo(0);
        transInfo = anim.GetAnimatorTransitionInfo(0);

        if (isKnockBacking)
        {
            agent.speed = -7;
            return;
        }
        if (target != null && agent.isOnNavMesh)
        {
            destinationPos = target.transform.position;
            agent.SetDestination(target.position);
            agent.speed = 7;
        }
        if (destinationPos != null && agent.isOnNavMesh)
            agent.SetDestination(destinationPos);

        if (Time.time > nextCheck)
        {
            nextCheck = Time.time + checkRate;
            CheckIfIShouldWander();
        }
        if (whoIsTheTarget != null)
        {
            float distance = Vector3.Distance(whoIsTheTarget.transform.position, transform.position);
            if (distance <= lookRadius)
            {
                if (target == null) target = whoIsTheTarget.transform;
                if (distance <= attackRange && !isKnockBacking)
                    DoAttack();
            }
            else if (distance > lookRadius)
            {
                if (target != null)
                {
                    target = null;
                    agent.speed = 2;
                }
                    
            }
        }
    }
    IEnumerator Dead()
    {
        yield return new WaitForSeconds(10);
        GameObject.Destroy(gameObject);
    }
    private void FixedUpdate()
    {
        base.FixedUpdate();
        if (health > 0)
        {
            AnimatorStateInfo info = anim.GetCurrentAnimatorStateInfo(0);

            for (int i = 0; i < 3; i++)
                if (info.IsName("Attack" + i))
                {
                    isAttacking = true;
                    break;
                }
            if (info.IsName("Dodging Back"))
                isKnockBacking = true;
            float rootSpeed = 0;
            Vector3 lookrotation = agent.steeringTarget - transform.position;
            if (IsInPivot())
                rootSpeed = 10;
            if (lookrotation != Vector3.zero)
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(lookrotation), rootSpeed * Time.deltaTime);
            charAngle = 0f;
            direction = 0f;

            // Translate controls stick coordinates into world/cam/character space
            StickToWorldspace(this.transform, ref direction, ref charAngle, IsInPivot());
            anim.SetFloat("Speed", agent.velocity.magnitude / 10 * 2, 0.05f, Time.deltaTime);
            anim.SetFloat("Direction", direction, 0.25f, Time.deltaTime);

            if (agent.velocity.magnitude > LocomotionThreshold)    // Dead zone
                if (!IsInPivot())
                    anim.SetFloat("Angle", charAngle);
            if (agent.velocity.magnitude < LocomotionThreshold)    // Dead zone
            {
                anim.SetFloat("Direction", 0f);
                anim.SetFloat("Angle", 0f);
            }
        }
    }
    private void DoAttack()
    {
        if (isAttacking)
            return;
        anim.CrossFadeInFixedTime("UpperBody.Attack" + Random.Range(1, 7), 0.1f);
        agent.velocity /= 2;
    }
    public void DoKnockBack()
    {
        if (isKnockBacking)
            return;
        anim.CrossFadeInFixedTime("Dodging Back", 0.01f);
        anim.CrossFadeInFixedTime("UpperBody.Dodging Back", 0.01f);
    }
    public void CheckIfIShouldWander()
    {
        if (target == null)
            if (RandomWanderTarget(t.position, wanderRange, out wanderTarget) && agent.isOnNavMesh)
                agent.SetDestination(wanderTarget);
    }

    bool RandomWanderTarget(Vector3 centre, float range, out Vector3 result)
    {
        Vector3 randomPoint = centre + Random.insideUnitSphere * wanderRange;
        if (NavMesh.SamplePosition(randomPoint, out navHit, 1f, NavMesh.AllAreas))
        {
            result = navHit.position;
            return true;
        }
        else
        {
            result = centre;
            return false;
        }
    }

    public void Spawn()
    {
        Vector3 spawnPos = player.gameObject.transform.position;
        spawnPos.x += UnityEngine.Random.Range(-40f, 40f);
        spawnPos.z += UnityEngine.Random.Range(-40f, 40f);
        while (Vector3.Distance(spawnPos, player.gameObject.transform.position) < 20)
        {
            spawnPos.x += UnityEngine.Random.Range(-40f, 40f);
            spawnPos.z += UnityEngine.Random.Range(-40f, 40f);

            new WaitForSeconds(2f);
        }
    }

    public override void takeDamage(float damage, HitPoint.HitParts hitPart)
    {

        base.takeDamage(damage, hitPart);
        //When Dead
        if (health <= 0 && anim.enabled)
        {
            StartCoroutine(Dead());
            if (skirt != null)
                skirt.GetComponent<Cloth>().damping = 1;
            int itemCount = 0;
            if (Random.Range(0, 10) > 6)
                itemCount = Random.Range(1, 2);
            if (armor != null && armor.active)
                itemCount += (int)(Random.Range(1, 5));
            if (itemCount < 0)
                itemCount = 0;

            for (int i = 0; i < itemCount; i++)
            {
                GameObject item = Instantiate(packtory.items[Random.Range(0, packtory.items.Length)], new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z), new Quaternion(0, 0, 0, 1));

                if (item.GetComponent<Item>().itemName != "")
                {
                    if (item.GetComponent<Item>() as ItemAmmo)
                    {
                        switch (item.GetComponent<ItemAmmo>().ammoType)
                        {
                            case ItemAmmo.AmmoTypes.MachineGun:
                                item.GetComponent<Item>().totalNum = Random.Range(10, 50);
                                break;
                            case ItemAmmo.AmmoTypes.Rifle:
                                item.GetComponent<Item>().totalNum = Random.Range(1, 20);
                                break;
                            case ItemAmmo.AmmoTypes.Pistol:
                                item.GetComponent<Item>().totalNum = Random.Range(1, 20);
                                break;
                        }
                    }
                    else if (item.GetComponent<Item>() as ItemGun)
                    {
                        if (!(zombieType == ZombieType.Captin && item.GetComponent<Item>().itemName == "mauiCP96" && Random.Range(0, 2) == 0))
                        {
                            Destroy(item);
                            i--;
                        }
                    }
                    else if (item.GetComponent<Item>() as ItemMelee)
                    {
                        if (item.GetComponent<Item>().itemName == "Axe")
                        {
                            if (!(zombieType == ZombieType.NonArmored && Random.Range(0, 10) < 9))
                            {
                                Destroy(item);
                                i--;
                            }
                        }
                        else if (item.GetComponent<Item>().itemName == "Knife")
                        {
                            if (zombieType == ZombieType.NonArmored)
                            {
                                Destroy(item);
                                i--;
                            }
                            else if (Random.Range(0, 10) < 6)
                            {
                                Destroy(item);
                                i--;
                            }
                        }
                        else if (item.GetComponent<Item>().itemName == "Pick Axe")
                        {
                            if (!(zombieType == ZombieType.NonArmored && Random.Range(0, 10) < 9))
                            {
                                Destroy(item);
                                i--;
                            }
                        }
                    }
                    else
                        item.GetComponent<Item>().totalNum = 1;
                }
                else
                {
                    Destroy(item);
                    i--;
                }
                item.GetComponent<Item>().DropItem();
            }

            anim.enabled = false;
            agent.enabled = false;
        }
    }
    void OnAnimatorIK(int layerIndex)
    {
        if (anim != null && anim.enabled && health > 0)
            if (target != null)
            {
                anim.SetLookAtWeight(0.7f);
                if (target.GetComponent<PlayerState>() == null)
                    anim.SetLookAtPosition(target.transform.position);
                else
                    anim.SetLookAtPosition(target.transform.GetChild(0).position);
            }
    }

    public float LocomotionThreshold { get { return 0.2f; } }
    public bool IsInPivot()
    {
        return stateInfo.IsName("loco.LocomotionPivotL") ||
            stateInfo.IsName("loco.LocomotionPivotR") ||
            transInfo.IsName("loco.Locomotion -> loco.LocomotionPivotL") ||
            transInfo.IsName("loco.Locomotion -> loco.LocomotionPivotR");
    }
    public void StickToWorldspace(Transform root, ref float directionOut, ref float angleOut, bool isPivoting)
    {
        Vector3 rootDirection = root.forward;
        Vector3 stickDirection = new Vector3(destinationPos.x, 0, destinationPos.z) - new Vector3(transform.position.x, 0, transform.position.z);
        // Convert joystick input in Worldspace coordinates
        Vector3 moveDirection = stickDirection;
        Vector3 axisSign = Vector3.Cross(moveDirection, rootDirection);
        float angleRootToMove = Vector3.Angle(rootDirection, moveDirection) * (axisSign.y >= 0 ? -1f : 1f);
        if (!isPivoting)
        {
            angleOut = angleRootToMove;
        }
        angleRootToMove /= 180f;

        directionOut = angleRootToMove * 1.5f;
    }
}
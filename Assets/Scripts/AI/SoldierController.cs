﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoldierController : MonoBehaviour
{
    Animator anim;
    AudioSource _AudioSource;
    public float vertical;
    public float horizontal;
    public bool crouch;
    public bool aim;
    public Transform rh;
    public bool fire;
    public ItemGun gun;

    private void Awake()
    {
        anim = GetComponent<Animator>();
        _AudioSource = GetComponent<AudioSource>();
    }
    private void Start()
    {
        StartCoroutine(FireGun());
    }
    // Update is called once per frame
    void Update()
    {
        anim.SetFloat("Vertical", vertical);
        anim.SetFloat("Horizontal", horizontal);
        if(vertical == 0 && horizontal == 0)
            anim.SetFloat("Speed", 0);
        else
            anim.SetFloat("Speed", 1);
        anim.SetBool("Crouch", crouch);
        anim.SetBool("Aim", aim);
    }
    void OnAnimatorIK(int layerIndex)
    {
        if (rh != null)
        {
            anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 1);
            anim.SetIKRotationWeight(AvatarIKGoal.RightHand, 1);
            anim.SetIKPosition(AvatarIKGoal.RightHand, rh.position);
            anim.SetIKRotation(AvatarIKGoal.RightHand, rh.rotation);
        }
    }
    IEnumerator FireGun()
    {
        while(true)
        {
            if(fire)
            {
                gun.muzzleFlash.Play();
                _AudioSource.PlayOneShot(gun.shootSound);
            }
            yield return new WaitForSecondsRealtime(gun.fireRate+Random.Range(gun.fireRate*0.2f, gun.fireRate));
        }
        
    }
}

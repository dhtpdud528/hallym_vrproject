﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneMoveManager : MonoBehaviour {

    public int sceneNum;
    public float waitTime;
    private void FixedUpdate()
    {
        StartCoroutine(WaitSeconds(waitTime));
    }
    IEnumerator WaitSeconds(float t)
    {
        yield return new WaitForSecondsRealtime(t);
        SceneManager.LoadScene(sceneNum);
    }
}

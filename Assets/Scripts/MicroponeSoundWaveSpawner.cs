﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[RequireComponent(typeof(AudioPeer))]
public class MicroponeSoundWaveSpawner : MonoBehaviour
{
    [SerializeField] GameObject wave;
    [SerializeField] Transform wavePoket;
    public AudioPeer audioPeer;
    [SerializeField] AudioMixerGroup microponeMixer;
    public float sensitivity;
    // Start is called before the first frame update
    private void Awake()
    {
        audioPeer = GetComponent<AudioPeer>();
    }
    void Start()
    {
        if (Microphone.devices.Length > 0)
        {
            audioPeer._audioSource.outputAudioMixerGroup = microponeMixer;
            audioPeer._audioSource.clip = Microphone.Start(Microphone.devices[0].ToString(),
                true, 1, 44100);
            while (!(Microphone.GetPosition(Microphone.devices[0].ToString()) > 0)) { }
            audioPeer._audioSource.Play();
            Debug.Log(Microphone.devices[0].ToString());
            StartCoroutine(spawnWaves());
        }
    }

    // Update is called once per frame
    void Update()
    {
    }
    IEnumerator spawnWaves()
    {
        while (true)
        //for (int i = 0; i < 8; i++)
        //{
        //    yield return new WaitForSecondsRealtime(.05f);
        //    if (audioPeer.spectrumsFreqBand[i] <= 0)
        //        continue;
        //    SoundWave spawnedWave = Instantiate(wave, transform.position, new Quaternion()).GetComponent<SoundWave>();
        //    spawnedWave.volume = audioPeer.spectrumsFreqBand[i] * 30;
        //    spawnedWave.transform.SetParent(wavePoket);
        //}
        {
            yield return new WaitForSecondsRealtime(.05f);
            if (audioPeer.volume > 0)
            {
                SoundWave spawnedWave = Instantiate(wave, transform.position, new Quaternion()).GetComponent<SoundWave>();
                spawnedWave.volume = audioPeer.volume * sensitivity;
                spawnedWave.transform.SetParent(wavePoket);
                spawnedWave.owner = gameObject;
            }
        }
    }
}

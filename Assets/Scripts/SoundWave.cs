﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundWave : MonoBehaviour
{
    public float volume;
    public float curVolume;
    public float speadSpeed;
    Material objMaterial;
    public GameObject owner;
    private bool hearMonster;
    public bool isVirtualSound;


    // Start is called before the first frame update
    void Start()
    {
        transform.localScale *= 0;
        objMaterial = GetComponent<Renderer>().material;
    }

    // Update is called once per frame
    void Update()
    {
        transform.localScale = new Vector3(transform.localScale.x + speadSpeed * Time.deltaTime, transform.localScale.y + speadSpeed * Time.deltaTime, transform.localScale.z + speadSpeed * Time.deltaTime);

        curVolume = 1 - transform.localScale.x / volume;
        if (curVolume <= 0)
            DestroyImmediate(gameObject);

    }
    private void OnTriggerEnter(Collider other)
    {
        if (!isVirtualSound && other.CompareTag("MonsterBody"))
        {
            if (owner != null && !owner.CompareTag("MonsterBody") && owner.CompareTag("Door") && (hearMonster || Vector3.Distance(other.transform.position, transform.position) <= 5))
                return;
            AI_Witch mon = other.GetComponent<AI_Witch>();
            hearMonster = true;
            Debug.Log(volume * curVolume);
            mon.notice += volume * curVolume;
            if (mon.notice > 50 && mon.ai_state == AI_Witch.STATE.Patrol)
            {
                mon.ai_state = AI_Witch.STATE.Alert;
                mon.m_HeadAudioSource.Stop();
                mon.PlayRandomAudio_Head(mon.alertSounds);
                mon.anim.Play("UpperBody.New State");
                mon.anim.CrossFadeInFixedTime("Alert", 0.1f);
                mon.agent.SetDestination(transform.position);
            }
            if (mon.ai_state == AI_Witch.STATE.Alert)
                mon.agent.SetDestination(transform.position);
            if (mon.notice > 120 && owner != null && owner.CompareTag("Player") && mon.ai_state != AI_Witch.STATE.Rush && mon.ai_state != AI_Witch.STATE.Run)
            {
                mon.m_HeadAudioSource.Stop();
                mon.anim.Play("UpperBody.New State");
                mon.anim.CrossFadeInFixedTime("Scream", 0.1f);
                mon.PlayRandomAudio_Head(mon.screamSounds);
                mon.ai_state = AI_Witch.STATE.Rush;
            }

        }
        if (other.GetComponent<ReflectionWave>() != null && other.GetComponent<ReflectionWave>().enabled)
            other.GetComponent<ReflectionWave>().spawnReflectionWaves(volume * curVolume);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemMelee : Item
{
    public float m_SwingSpeed;
    public enum MeleeTypes { Light = 0, Heavy = 1 }
    public MeleeTypes types;
    public bool canSwing;
    public string[] useFullChargeAnimations;
    public float damegeOrigin { get; set; }
    public float damege;
    public bool chargedMulti;
    public bool quickMulti;
    public float needStemina;

    public AudioClip[] swingSounds;
    public AudioClip[] hitSounds;
    private AudioSource m_AudioSource;
    private void Start()
    {
        m_AudioSource = GetComponent<AudioSource>();
        if (needStemina < 5)
            needStemina = 5;
        canSwing = true;
        damegeOrigin = damege;
    }
    public override void PrepareItem()
    {
        base.PrepareItem();
        weaponCon.animWeapon.SetFloat("SwingSpeed", m_SwingSpeed);
        playerCon.usingItem = true;

    }
    public void SwingMelee()
    {
        
        PlayRandomAudio(swingSounds);
        playerCon.usingItem = false;
        canSwing = false;
        if (hudProgressBar.imageProgressBar.fillAmount >= 1)
        {
            damege = damegeOrigin*3;
            DoFullCharge();
        }
        else
        {
            damege = damegeOrigin;
            DoUse();
        }
    }
    public void DoFullCharge()
    {
        preparingItem = false;
        canSwing = false;
        hudProgressBar.imageProgressBar.fillAmount = 0;
        if (useFullChargeAnimations.Length > 0)
            weaponCon.animWeapon.CrossFadeInFixedTime(useFullChargeAnimations[UnityEngine.Random.Range(0, useFullChargeAnimations.Length)], 0.01f);
    }
    public void PlayRandomAudio(AudioClip[] audios)
    {
        // pick & play a random footstep sound from the array,
        // excluding sound at index 0
        int n = Random.Range(1, audios.Length);
        m_AudioSource.clip = audios[n];
        m_AudioSource.PlayOneShot(m_AudioSource.clip);
        // move picked sound to index 0 so it's not picked next time
        audios[n] = audios[0];
        audios[0] = m_AudioSource.clip;
    }
}

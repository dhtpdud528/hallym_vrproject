﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemGun : Item
{
    public float recoil;
    public float MOA;//집탄률
    public float fireRate;
    public int bulletsPerMag;
    public float reloadSpeed;
    public int currentBlullets;
    public float damage;
    public float noise;

    public float recoilOrigin;
    public float MOAOrigin;//집탄률
    public float fireRateOrigin;
    public int bulletsPerMagOrigin;
    public float reloadSpeedOrigin;
    public float damageOrigin;
    public float noiseOrigin;

    public ParticleSystem muzzleFlash;
    public AudioClip shootSound;
    public AudioClip lastShootSound;

    private const int ReloadSoundsIndex = 4;
    public AudioClip[] ReloadSound = new AudioClip[ReloadSoundsIndex];

    public enum fireTypes { Auto, Semi_Auto };
    public fireTypes fireType;

    public ItemAmmo.AmmoTypes ammoType;

    private void Start()
    {
        recoilOrigin = recoil;
        MOAOrigin = MOA;
        fireRateOrigin = fireRate;
        bulletsPerMagOrigin = bulletsPerMag;
        reloadSpeedOrigin = reloadSpeed;
        damageOrigin = damage;
        noiseOrigin = noise;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemFirstAidKit : Item
{
    private PlayerInv p_inv_2;
    public bool use = false;
    public override void PrepareItem()
    {
        base.PrepareItem();
        playerCon.usingItem = true;
        if (hudProgressBar.imageProgressBar.fillAmount >= 1)
            use = true;
    }
    public override void CancelUsing()
    {
        base.CancelUsing();
        playerCon.usingItem = false;
    }
    private void Start()
    {
        p_inv_2 = FindObjectOfType<PlayerInv>();
    }
    protected void OnDisable()
    {
        base.OnDisable();
        playerCon.usingItem = false;
        use = false;
    }
    // Update is called once per frame
    void Update()
    {
        if (use)
        {
            if (p_inv.GetCurrentItemInfo() == gameObject.GetComponent<Item>())
                hudProgressBar.imageProgressBar.fillAmount = 0;
            totalNum--;
            p_inv_2.UIUpdate();
            if (p_inv_2.GetCurrentItemInfo() != gameObject.GetComponent<Item>())
            {
                playerState.bloody = false;
                playerCon.usingItem = false;
                playerState.AddHealth(10);
                GameObject.Destroy(gameObject);
            }
        }
    }
}

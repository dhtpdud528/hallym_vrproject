﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
public class Item : MonoBehaviour
{
    public Sprite icon;
    protected Rigidbody itemRigdbody;
    protected Collider itemboxCollider;
    protected WeaponController weaponCon;
    protected PlayerInv p_inv;
    protected PlayerState playerState;
    protected VRSoundFirstPersonController playerCon;
    protected HUDProgressBar hudProgressBar;
    public float MaxReadyTime = 1;
    public string itemName = "";
    public float i_Weight = 0.03f;
    public int totalNum;
    public int maxNum = 1;
    public int id;
    public Vector3 HandPosition;
    public Vector3 HandRotation;
    public float drawSpeed = 1;
    protected bool preparingItem;
    public string[] prepareAnimations;
    public string[] useAnimations;
    public int maxHit;
    public int scraps;

    public int modCost;
    public int modedCount;
    public int MaxModCount;
    public bool isModed;
    public List<string> modedList;

    protected void Awake()
    {
        modedCount = 0;
        if (maxHit <= 0)
            maxHit = 1;
        if (totalNum <= 0)
            totalNum = 1;
        playerCon = FindObjectOfType<VRSoundFirstPersonController>();
        playerState = FindObjectOfType<PlayerState>();
        p_inv = FindObjectOfType<PlayerInv>();
        hudProgressBar = FindObjectOfType<HUDProgressBar>();
        weaponCon = FindObjectOfType<WeaponController>();
        itemRigdbody = GetComponent<Rigidbody>();
        itemboxCollider = GetComponent<Collider>();
        id = UnityEngine.Random.Range(0, 100000);
    }
    public virtual void PrepareItem()
    {
        if (playerState.down)
        {
            hudProgressBar.imageProgressBar.fillAmount = 0;
            return;
        }
        if (p_inv.GetCurrentItemInfo() == gameObject.GetComponent<Item>())
            if (hudProgressBar.imageProgressBar.fillAmount < 1)
                DoPrepare();
    }
    public bool IsFullProgressBar()
    {
        if (hudProgressBar.imageProgressBar.fillAmount >= 1)
            return true;
        else
            return false;
    }
    public bool IsProgressingBar()
    {
        if (hudProgressBar.imageProgressBar.fillAmount > 0)
            return true;
        else
            return false;
    }
    public void DoPrepare()
    {
        hudProgressBar.imageProgressBar.fillAmount += Time.deltaTime / MaxReadyTime;
        if (preparingItem) return;
        preparingItem = true;
        if (prepareAnimations.Length > 0)
            weaponCon.animWeapon.CrossFadeInFixedTime(prepareAnimations[UnityEngine.Random.Range(0, prepareAnimations.Length)], 0.01f);
    }
    public void DoUse()
    {
        hudProgressBar.imageProgressBar.fillAmount = 0;
        playerCon.usingItem = false;
        preparingItem = false;
        if (useAnimations.Length > 0)
            weaponCon.animWeapon.CrossFadeInFixedTime(useAnimations[UnityEngine.Random.Range(0, useAnimations.Length)], 0.01f);
    }


    public virtual void CancelUsing()
    {
        if (p_inv.GetCurrentItemInfo() == gameObject.GetComponent<Item>())
            hudProgressBar.imageProgressBar.fillAmount = 0;
        playerCon.usingItem = false;
        preparingItem = false;
    }
    public virtual void DropItem_fromPlayer()
    {
        if (itemName == "") return;
        transform.SetParent(null);
        gameObject.transform.position = weaponCon.shootPoint.position;
        gameObject.SetActive(true);
        itemRigdbody.freezeRotation = false;
        itemRigdbody.useGravity = true;
        itemboxCollider.enabled = true;
        itemRigdbody.AddForce(weaponCon.shootPoint.forward * 200, ForceMode.Acceleration);
    }
    public void DropItem()
    {
        if (itemName == "") return;
        transform.SetParent(null);
        gameObject.SetActive(true);
        itemRigdbody.freezeRotation = false;
        itemRigdbody.useGravity = true;
        itemboxCollider.enabled = true;
    }
    public virtual void pickUpItem()
    {
        itemRigdbody.freezeRotation = true;
        itemRigdbody.velocity *= 0;
        itemRigdbody.useGravity = false;
        itemboxCollider.enabled = false;
    }
    protected virtual void OnDisable()
    {
        if (hudProgressBar  != null && hudProgressBar.imageProgressBar != null)
            hudProgressBar.imageProgressBar.fillAmount = 0;
        preparingItem = false;
    }
    public void AddTotalNum(int i)
    {
        this.totalNum += i;
        if (totalNum <= 0)
        {
            if (p_inv.SearchItem_inBackpack(GetComponent<Item>()) != null)
                p_inv.backpackInv.Remove(gameObject);
            GameObject.Destroy(gameObject);
        }
    }
    public IEnumerator StartDestroy()
    {
        yield return new WaitForSecondsRealtime(0.05f);
        Destroy(gameObject);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemAmmo : Item {
    public enum AmmoTypes {
        MachineGun  =0,
        Rifle       =1,
        Pistol      =2}
    public AmmoTypes ammoType;

    public void updateAmmosWeight()
    {
        switch (ammoType)
        {
            case AmmoTypes.MachineGun:
                i_Weight = 0.009f * totalNum;
                break;
            case AmmoTypes.Rifle:
                i_Weight = 0.005f * totalNum;
                break;
            case AmmoTypes.Pistol:
                i_Weight = 0.003f * totalNum;
                break;
        }
    }
    private void Start()
    {
        updateAmmosWeight();
    }
}

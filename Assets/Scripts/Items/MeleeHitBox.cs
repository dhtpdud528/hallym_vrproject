﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeHitBox : MonoBehaviour
{
    public WeaponController weaponCon;
    public PlayerInv playerInv;
    public float damage;
    public int maxStack;
    public int currentStack;
    public List<int> ids = new List<int>();
    // Start is called before the first frame update
    private void Awake()
    {
        playerInv = FindObjectOfType<PlayerInv>();
    }
    void Start()
    {
        for (int i = 0; i < GetComponents<BoxCollider>().Length; i++)
            GetComponents<BoxCollider>()[i].enabled = false;
        weaponCon = FindObjectOfType<WeaponController>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (currentStack <= maxStack)
        {
            if(other.GetComponent<HitPoint>() != null && !MultiHitCheck(other.GetComponent<HitPoint>().state.id))
            {
                ItemMelee melee = (ItemMelee)playerInv.GetCurrentItemInfo();
                melee.PlayRandomAudio(melee.hitSounds);
                if (weaponCon.isMeleeAttacking && other.GetComponent<HitPoint>().state.GetComponent<AI_Zombie>() != null)
                    other.GetComponent<HitPoint>().state.GetComponent<AI_Zombie>().DoKnockBack();
                other.GetComponent<HitPoint>().giveDamage(damage);
                ids.Add(other.GetComponent<HitPoint>().state.id);
                currentStack++;
                if (other.GetComponent<Rigidbody>() != null)
                    other.GetComponent<Rigidbody>().AddForce(transform.forward * 500 * damage, ForceMode.Acceleration);
            }
            else if (other.GetComponent<Door>() != null && other.GetComponent<Door>().lockDoor)
            {
                Door door = other.GetComponent<Door>();
                door.PlayRandomAudio(door.breakSounds);
                door.TakeDamage(damage);
                //ids.Add(other.GetComponent<Door>().id);
            }
        }
        

    }
    private bool MultiHitCheck(int id)
    {
        bool found = false;
        for (int i = 0; i < ids.Count; i++)
        {
            if (ids[i] == id)
            {
                found = true;
                break;
            }
        }
        return found;
    }
}

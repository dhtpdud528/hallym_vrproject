﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(WaveSpawner))]
public class ItemStone : Item
{
    private AudioSource audioSource;
    public AudioClip[] rockSounds;
    public bool canThrow;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }
    protected override void OnDisable()
    {
        base.OnDisable();
        canThrow = false;
    }
    public override void PrepareItem()
    {
        base.PrepareItem();
        if (hudProgressBar.imageProgressBar.fillAmount >= 1)
            canThrow = true;
    }
    public void ThrowItem(Vector3 dir)
    {
        transform.SetParent(null);
        if (p_inv.GetCurrentItemInfo() == gameObject.GetComponent<Item>())
            DoUse();
        hudProgressBar = null;
        totalNum--;
        itemRigdbody.useGravity = true;
        itemboxCollider.enabled = true;
        itemRigdbody.AddForce(dir * 1000, ForceMode.Acceleration);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (itemRigdbody.velocity.magnitude>1)
            StartCoroutine(BreakStone());
    }
    protected void PlayRandomAudio(AudioClip[] audios)
    {
        // pick & play a random footstep sound from the array,
        // excluding sound at index 0
        int n = Random.Range(1, audios.Length);
        audioSource.clip = audios[n];
        audioSource.PlayOneShot(audioSource.clip);
        // move picked sound to index 0 so it's not picked next time
        audios[n] = audios[0];
        audios[0] = audioSource.clip;
    }
    IEnumerator BreakStone()
    {
        PlayRandomAudio(rockSounds);
        GetComponent<Renderer>().enabled = false;
        while (audioSource.isPlaying)
            yield return new WaitForSeconds(.1f);
        Destroy(gameObject);
    }
}

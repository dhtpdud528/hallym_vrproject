﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemList : MonoBehaviour
{
    public GameObject[] items;
    public GameObject FindItem(string name)
    {
        for(int i= 0; i<items.Length;i++)
        {
            if (name == items[i].GetComponent<Item>().itemName)
                return items[i];
        }
        return null;
    }
    private void Start()
    {
        StartCoroutine(checkMeleeItems());
    }
    public Item[] SearchSameItems(int id, string itemName)
    {
        Item[] items = FindObjectsOfType<Item>();
        List<Item> tempItems = new List<Item>();
        for (int i = 0; i < items.Length; i++)
            if (items[i].id != id && items[i].itemName == itemName)
                tempItems.Add(items[i]);
        return tempItems.ToArray();
    }

    IEnumerator checkMeleeItems()
    {
        yield return new WaitForFixedUpdate();
        yield return new WaitForSeconds(0.5f);
        ItemBox[] itemBoxes = FindObjectsOfType<ItemBox>();
        ItemMelee axe;
        ItemMelee pickaxe;
        ItemMelee knife;
        itemBoxes[Random.Range(0, itemBoxes.Length)].BoxInv.Add(Instantiate(FindItem("도끼")));

        itemBoxes[Random.Range(0, itemBoxes.Length)].BoxInv.Add(Instantiate(FindItem("곡괭이")));
        itemBoxes[Random.Range(0, itemBoxes.Length)].BoxInv.Add(Instantiate(FindItem("칼")));
    }
}

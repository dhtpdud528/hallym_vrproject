﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBox : MonoBehaviour
{
    public List<GameObject> BoxInv = new List<GameObject>();
    public bool ammoBox;
    private ItemList packtory;
    public AudioClip OpenSounds;
    public AudioSource audioSource;


    // Use this for initialization
    void Start()
    {
        packtory = FindObjectOfType<ItemList>();
        audioSource = GetComponent<AudioSource>();

        int itemCount = Random.Range(1, 6);
        if (itemCount < 0)
            itemCount = 0;
        for (int i = 0; i < itemCount; i++)
        {
            GameObject item = Instantiate(packtory.items[Random.Range(0, packtory.items.Length)],new Vector3(0,-999,0), new Quaternion());
            if (item.GetComponent<Item>().itemName != "")
            {
                if (item.GetComponent<Item>() as ItemAmmo)
                {
                    switch (item.GetComponent<ItemAmmo>().ammoType)
                    {
                        case ItemAmmo.AmmoTypes.MachineGun:
                            item.GetComponent<Item>().totalNum = Random.Range(20, 200);
                            break;
                        case ItemAmmo.AmmoTypes.Rifle:
                            item.GetComponent<Item>().totalNum = Random.Range(1, 20);
                            break;
                        case ItemAmmo.AmmoTypes.Pistol:
                            item.GetComponent<Item>().totalNum = Random.Range(1, 20);
                            break;
                    }
                    AddItem(item);
                }
                else if (item.GetComponent<Item>() as ItemMelee)
                {
                    Destroy(item);
                    i--;
                }
                else if (item.GetComponent<Item>() as ItemGun)
                {
                    Destroy(item);
                    i--;
                }
                else if (item.GetComponent<Item>().itemName == "문따개" && Random.Range(0, 10) > 5)
                {
                    Destroy(item);
                    i--;
                }
                else if (item.GetComponent<Item>().itemName == "마스터 키")
                {
                    Destroy(item);
                    i--;
                }
                else
                    AddItem(item);
            }
            else
                i--;
        }
        
    }
    public void AddItem(GameObject item)
    {
        if (item.GetComponent<Item>() is ItemAmmo)
        {
            ItemAmmo searchedAmmo = searchAmmo(item.GetComponent<ItemAmmo>().ammoType);
            if (searchedAmmo != null)
            {
                searchedAmmo.totalNum += item.GetComponent<ItemAmmo>().totalNum;
                Destroy(item);
            }
            else
            {
                item.GetComponent<Item>().pickUpItem();
                BoxInv.Add(item);
            }
        }
        else
        {
            item.transform.SetParent(transform);
            BoxInv.Add(item);
        }
        

    }
    public ItemAmmo searchAmmo(ItemAmmo.AmmoTypes search)
    {
        GameObject find = null;
        if (BoxInv.Count == 0)
            return null;
        for (int i = 0; i < BoxInv.Count; i++)
        {
            if (BoxInv[i] != null && BoxInv[i].GetComponent<Item>() is ItemAmmo && BoxInv[i].GetComponent<ItemAmmo>().ammoType == search)
            {
                find = BoxInv[i];
                break;
            }
        }
        if (find != null)
            return find.GetComponent<ItemAmmo>();
        else
            return null;
    }
    
    // Update is called once per frame
    void Update()
    {
        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class EntityBullet : Entity
{
    public ItemGun gun;
    [SerializeField] private GameObject rockBulletImpact;
    [SerializeField] private GameObject bodyBulletImpact;
    private float lifeTime;

    // Use this for initialization
    void Start()
    {
        //GetComponent<Rigidbody>().mass += gun.damage/4;
    }

    // Update is called once per frame
    void Update()
    {
        lifeTime += Time.deltaTime;
        if (lifeTime >= 3)
            GameObject.Destroy(gameObject);
    }
    private void OnCollisionEnter(Collision collision)
    {
        //GameObject bulletImpact = rockBulletImpact;
        if (collision.collider.CompareTag("Monster"))
        {
            //bulletImpact = bodyBulletImpact;
            collision.gameObject.
                GetComponent<HitPoint>().
                giveDamage(gun.damage);
            GameObject.Destroy(gameObject);
        }
        //GameObject bulletHole = Instantiate(bulletImpact, collision.GetContact(0).point, Quaternion.FromToRotation(Vector3.forward, collision.GetContact(0).normal));
        //bulletHole.transform.SetParent(collision.transform);
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Monster"))
        {
            Debug.Log(other.name);
            //bulletImpact = bodyBulletImpact;
            other.
                GetComponent<HitPoint>().
                giveDamage(gun.damage);
            GameObject.Destroy(gameObject);
        }
        else if (other.CompareTag("Door") && other.gameObject.GetComponent<Door>().lockDoor)
        {
            Door door = other.gameObject.GetComponent<Door>();
            door.TakeDamage(gun.damage);
            door.PlayRandomAudio(door.shotedSounds);
            GameObject.Destroy(gameObject);
        }
    }
}

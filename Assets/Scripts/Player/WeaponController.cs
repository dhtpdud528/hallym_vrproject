﻿using System;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class WeaponController : MonoBehaviour
{
    public bool isCinematic;
    public AudioClip HitMarkSFX;
    public Animator animWeapon;
    private Animator animBody;
    public AudioSource _AudioSource;
    private PlayerInv playerInv;
    private PlayerState p_state;
    public VRSoundFirstPersonController playerCon;
    private CharacterController characterCon;
    public Camera maincam;

    public float range = 100f;

    public Transform shootPoint;
    private float fireTimer = 0;
    public bool isReloading;
    public bool isMeleeAttacking;

    public ItemGun gun;
    private HUDStateInfo hud;

    public Vector3 aimPos;
    public Vector3 originPos;
    public float aodSpeed;

    [SerializeField] private GameObject rockBulletImpact;
    [SerializeField] private GameObject bodyBulletImpact;
    public bool isAiming;


    [SerializeField] private GameObject bullet;
    float reloadSpeed;

    private void Awake()
    {
    }
    // Use this for initialization
    void Start()
    {
        animBody = FindObjectOfType<PlayerAnimController>().GetComponent<Animator>();
        _AudioSource = GetComponent<AudioSource>();
        maincam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        playerCon = FindObjectOfType<VRSoundFirstPersonController>();

        characterCon = FindObjectOfType<CharacterController>();
        hud = FindObjectOfType<HUDStateInfo>();
        animWeapon = GetComponent<Animator>();
        playerInv = GetComponent<PlayerInv>();
        p_state = FindObjectOfType<PlayerState>();
        originPos = playerInv.hand.transform.parent.localPosition;
    }

    private int checkLeftAmmo(ItemAmmo.AmmoTypes ammoType)
    {
        return playerInv.SearchAmmo(gun.ammoType).GetComponent<Item>().totalNum;
    }

    // Update is called once per frame
    void Update()
    {
        if (isCinematic) return;
        RaycastHit hit;
        if (!Physics.Raycast(shootPoint.position, shootPoint.transform.forward, out hit, 3, (1 << 5) + (1 << 9) + (1 << 16)))
        {
            if (Input.GetKeyDown(KeyCode.V) && !playerInv.isSwaping)//밀리어택
                DoMelee(1);
            if (playerInv.GetCurrentItemInfo() is ItemGun && !playerInv.touchUI.active)
            {
                bool shootingMode = false;

                if (gun.fireType == ItemGun.fireTypes.Auto)
                    shootingMode = Input.GetButton("Fire1");
                else
                    shootingMode = Input.GetButtonDown("Fire1");
                if (shootingMode)
                {
                    if (!playerInv.isSwaping)
                    {
                        if (gun.currentBlullets > 0)
                            Fire();
                        else if (playerInv.SearchAmmo(gun.ammoType).GetComponent<Item>().totalNum > 0)
                        {
                            DoReload();
                            gun.muzzleFlash.Stop();
                        }
                        else
                            gun.muzzleFlash.Stop();
                    }
                }
                else
                    gun.muzzleFlash.Stop();

                if (fireTimer < gun.fireRate)
                    fireTimer += Time.deltaTime;
            }
            else if (playerInv.GetCurrentItemInfo() is ItemFirstAidKit && !playerInv.touchUI.active)
            {
                if (Input.GetButton("Fire1") && p_state.GetHealth() < p_state.maxHeath)
                    ((ItemFirstAidKit)playerInv.GetCurrentItemInfo()).PrepareItem();
                else if (Input.GetButtonUp("Fire1"))
                {
                    ((ItemFirstAidKit)playerInv.GetCurrentItemInfo()).CancelUsing();
                    hud.UpdateHUDInfo();
                    playerInv.UIUpdate();
                }
            }
            else if (playerInv.GetCurrentItemInfo() is ItemMelee)
            {
                if (Input.GetButton("Fire1") && ((ItemMelee)playerInv.GetCurrentItemInfo()).canSwing && !playerInv.isSwaping)
                    ((ItemMelee)playerInv.GetCurrentItemInfo()).PrepareItem();
                else if (Input.GetButtonUp("Fire1") && ((ItemMelee)playerInv.GetCurrentItemInfo()).canSwing && !playerInv.isSwaping && p_state.stemina < p_state.maxStemina)
                {
                    p_state.stemina += ((ItemMelee)playerInv.GetCurrentItemInfo()).needStemina;
                    p_state.AddRelax(-2 * ((ItemMelee)playerInv.GetCurrentItemInfo()).needStemina);
                    ((ItemMelee)playerInv.GetCurrentItemInfo()).SwingMelee();
                }
            }
            else if (playerInv.GetCurrentItemInfo() is ItemStone && !playerInv.touchUI.active)
            {
                ItemStone itemStone = ((ItemStone)playerInv.GetCurrentItemInfo());
                if (Input.GetButton("Fire1"))
                    itemStone.PrepareItem();
                else if (Input.GetButtonUp("Fire1"))
                {
                    if (itemStone.canThrow)
                    {
                        itemStone.ThrowItem(shootPoint.transform.forward);
                        hud.UpdateHUDInfo();
                        playerInv.UIUpdate();
                    }
                    else
                        itemStone.CancelUsing();
                }

            }
        }
    }

    internal void PlayHitMark()
    {
        _AudioSource.PlayOneShot(HitMarkSFX);
    }

    private void FixedUpdate()
    {
        AnimatorStateInfo infoHand = animWeapon.GetCurrentAnimatorStateInfo(0);

        reloadSpeed = gun.reloadSpeed;
        isReloading = infoHand.IsName("Reload");
        isMeleeAttacking = infoHand.IsName("Melee");
        animWeapon.SetFloat("ReloadSpeed", reloadSpeed);
    }

    private void Fire()
    {
        if (fireTimer < gun.fireRate || gun.currentBlullets <= 0 || isMeleeAttacking || isReloading) return;
        gun.currentBlullets--;
        if (gun.lastShootSound != null && gun.currentBlullets <= 0)
            _AudioSource.PlayOneShot(gun.lastShootSound);
        _AudioSource.PlayOneShot(gun.shootSound);


        int layerMask = (1 << 12);
        Collider[] hitColliders = Physics.OverlapSphere(playerCon.transform.position, 200, layerMask);
        for (int i = 0; i < hitColliders.Length; i++)
        {
            if (hitColliders[i].GetComponent<AI_Zombie>() != null)
                hitColliders[i].GetComponent<AI_Zombie>().destinationPos = playerCon.transform.position;
        }

        Vector3 shootDir = shootPoint.transform.forward;
        int aim = 1;
        if (isAiming)
            aim = 2;
        float moveSpeed = characterCon.velocity.magnitude / 200;

        shootDir.z += UnityEngine.Random.Range(-((ItemGun)playerInv.GetCurrentItemInfo()).MOA / aim - moveSpeed, ((ItemGun)playerInv.GetCurrentItemInfo()).MOA / aim + moveSpeed);
        shootDir.x += UnityEngine.Random.Range(-((ItemGun)playerInv.GetCurrentItemInfo()).MOA / aim - moveSpeed, ((ItemGun)playerInv.GetCurrentItemInfo()).MOA / aim + moveSpeed);
        shootDir.y += UnityEngine.Random.Range(-((ItemGun)playerInv.GetCurrentItemInfo()).MOA / aim - moveSpeed, ((ItemGun)playerInv.GetCurrentItemInfo()).MOA / aim + moveSpeed);
        GameObject bulletobj = Instantiate(bullet, shootPoint.position, Quaternion.FromToRotation(Vector3.forward, shootPoint.forward));
        bulletobj.GetComponent<EntityBullet>().gun = gun;
        bulletobj.GetComponent<Rigidbody>().AddForce(shootDir * 10000, ForceMode.Acceleration);
        /*if (Physics.Raycast(shootPoint.position, shootDir, out hit, range, (1 << 10) + (1 << 0) + (1 << 12) + (1 << 14)))
        {
            GameObject bulletImpact;
            if (hit.collider.gameObject.tag == "Monster")
                bulletImpact = bodyBulletImpact;
            else
                bulletImpact = rockBulletImpact;
            GameObject bulletHole = Instantiate(bulletImpact, hit.point, Quaternion.FromToRotation(Vector3.forward, hit.normal));
            bulletHole.transform.SetParent(hit.transform);
        }*/

        animWeapon.CrossFadeInFixedTime("Fire", 0.01f);
        gun.muzzleFlash.Play();
        //PlayShootSound();


        fireTimer = 0;
        //maincam.GetComponent<CameraShaker>().ShakeOnce(gun.recoil, gun.recoil, 0, 0.3f);
        hud.UpdateHUDInfo();
    }

    private void DoMelee(int damage)
    {
        if (isMeleeAttacking) return;
        p_state.stemina += 10;
        p_state.AddRelax(-10);
        animWeapon.CrossFadeInFixedTime("Melee", 0.01f);
    }

    public void DoReload()
    {
        if (isReloading || isMeleeAttacking) return;
        animWeapon.CrossFadeInFixedTime("Reload", 0.01f);
        animBody.CrossFadeInFixedTime("Reload", 0.01f);
    }

    public void Reload()
    {
        gun.muzzleFlash.Stop();
        if (playerInv.SearchAmmo(gun.ammoType).GetComponent<Item>().totalNum <= 0) return;
        int bulletsToLoad = gun.bulletsPerMag - gun.currentBlullets;
        int bulletsToDeduct = (playerInv.SearchAmmo(gun.ammoType).GetComponent<Item>().totalNum >= bulletsToLoad) ? bulletsToLoad : playerInv.SearchAmmo(gun.ammoType).GetComponent<Item>().totalNum;

        playerInv.SearchAmmo(gun.ammoType).GetComponent<Item>().totalNum -= bulletsToDeduct;
        gun.currentBlullets += bulletsToDeduct;
        hud.UpdateHUDInfo();
    }

    private void PlayShootSound()
    {
        _AudioSource.PlayOneShot(gun.shootSound);
    }

}

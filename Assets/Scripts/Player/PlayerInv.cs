﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(WeaponController))]
public class PlayerInv : MonoBehaviour
{
    public PlayerAnimController animCon;
    private WeaponController weaponCon;
    private Animator anim;
    public GameObject hand;
    public ItemList packtory;
    public List<GameObject> backpackInv = new List<GameObject>();
    public GameObject[] quickInv = new GameObject[4];
    private float weight;
    public int currentAdress = 0;
    private HUDStateInfo hudStateInfo;
    private HUDProgressBar hudProgressBar;
    public int nextIndex;
    public int previousIndex;
    public bool isSwaping;
    public int scraps;
    public int wheelIndex;
    public GameObject touchUI;
    public UIReporter HUDReport;
    UIInv uiInv;

    // Use this for initialization
    private void Awake()
    {
        uiInv = FindObjectOfType<UIInv>();
        HUDReport = FindObjectOfType<UIReporter>();
        animCon = FindObjectOfType<PlayerAnimController>();

        hudProgressBar = FindObjectOfType<HUDProgressBar>();
        weaponCon = FindObjectOfType<WeaponController>();
        anim = GetComponent<Animator>();
        packtory = FindObjectOfType<ItemList>();
        hand = GameObject.Find("Hand");
        hudStateInfo = FindObjectOfType<HUDStateInfo>();
    }
    void Start()
    {
        //uiItemBox.gameObject.SetActive(false);
        GameObject ammo;
        ammo = Instantiate(packtory.FindItem("7.62mm탄"));
        ammo.GetComponent<ItemAmmo>().totalNum = 14;
        backpackInv.Add(ammo);

        quickInv[0] = Instantiate(packtory.FindItem("M1 Carbine"));
        quickInv[1] = Instantiate(packtory.FindItem(""));
        quickInv[2] = Instantiate(packtory.FindItem("문따개"));
        quickInv[3] = Instantiate(packtory.FindItem("붕대"));
        quickInv[4] = Instantiate(packtory.FindItem("돌맹이"));

        for (int i = 0; i < 10; i++)
            backpackInv.Add(Instantiate(packtory.FindItem("돌맹이")));
        scraps = int.MaxValue;

        UIUpdate();
        hudStateInfo.UpdateHUDInfo();
        QuickInvInitialize();
    }



    private void FixedUpdate()
    {
        //QuickInvUpdate();
        for (int i = 0; i < backpackInv.Count; i++)
            backpackInv[i].transform.position = new Vector3(0, -999, 0);
    }
    public void EquipItemToHand(GameObject item)
    {
        if (item.GetComponent<Item>() is ItemGun)
        {
            int index = 0;

            if (quickInv[0].GetComponent<Item>().itemName == "")
                index = 0;

            if (quickInv[index].GetComponent<Item>().itemName != "")
            {
                quickInv[index].GetComponent<Item>().DropItem_fromPlayer();
                quickInv[index].transform.SetParent(null);
                backpackInv.Add(quickInv[index]);
                quickInv[index].GetComponent<Item>().pickUpItem();
            }
            //dropItem(quickInv[index]);
            else
                GameObject.Destroy(quickInv[index]);
            quickInv[index] = item;
            GrapItem(quickInv[index]);
            quickInv[index].GetComponent<Item>().pickUpItem();
            DoSwap(index);
        }
        else if (item.GetComponent<Item>() is ItemMelee)
        {
            int index = 1;

            if (quickInv[index].GetComponent<Item>().itemName != "")
            {
                quickInv[index].GetComponent<Item>().DropItem_fromPlayer();
                quickInv[index].transform.SetParent(null);
                PickUpItemToinv(quickInv[index]);
            }
            //dropItem(quickInv[index]);
            else
                GameObject.Destroy(quickInv[index]);
            quickInv[index] = item;
            GrapItem(quickInv[index]);
            quickInv[index].GetComponent<Item>().pickUpItem();
            DoSwap(index);
        }
        else if (item.GetComponent<Item>() is ItemFirstAidKit)
        {
            int index = 3;

            if (quickInv[index].GetComponent<Item>().itemName != "")
            {
                quickInv[index].GetComponent<Item>().DropItem_fromPlayer();
                quickInv[index].transform.SetParent(null);
                PickUpItemToinv(quickInv[index]);
            }
            //dropItem(quickInv[index]);
            else
                GameObject.Destroy(quickInv[index]);
            quickInv[index] = item;
            GrapItem(quickInv[index]);
            quickInv[index].GetComponent<Item>().pickUpItem();
            DoSwap(index);
        }
        else if (item.GetComponent<Item>() is ItemStone)
        {
            int index = 4;

            if (quickInv[index].GetComponent<Item>().itemName != "")
            {
                quickInv[index].GetComponent<Item>().DropItem_fromPlayer();
                quickInv[index].transform.SetParent(null);
                PickUpItemToinv(quickInv[index]);
            }
            //dropItem(quickInv[index]);
            else
                GameObject.Destroy(quickInv[index]);
            quickInv[index] = item;
            GrapItem(quickInv[index]);
            quickInv[index].GetComponent<Item>().pickUpItem();
            DoSwap(index);
        }
        else if (item.GetComponent<Item>().itemName == "문따개")
        {
            int index = 2;

            if (quickInv[index].GetComponent<Item>().itemName != "")
            {
                quickInv[index].GetComponent<Item>().DropItem_fromPlayer();
                quickInv[index].transform.SetParent(null);
                PickUpItemToinv(quickInv[index]);
            }
            //dropItem(quickInv[index]);
            else
                GameObject.Destroy(quickInv[index]);
            quickInv[index] = item;
            GrapItem(quickInv[index]);
            quickInv[index].GetComponent<Item>().pickUpItem();
            DoSwap(index);
        }
    }
    public void PickUpItemToinv(GameObject item, MonoBehaviour parent)
    {
        if (item.GetComponent<Item>() is ItemGun && quickInv[0].GetComponent<Item>().itemName != "" && quickInv[1].GetComponent<Item>().itemName != "")
        {
            backpackInv.Add(item);
            item.GetComponent<Item>().pickUpItem();
        }
        else
            PickUpItemToinv(item);
    }
    public void PickUpItemToinv(GameObject item)
    {
        if (item.GetComponent<Item>() is ItemGun)
        {
            int index = 0;

            if (quickInv[0].GetComponent<Item>().itemName == "")
                index = 0;
            else if (quickInv[1].GetComponent<Item>().itemName == "")
                index = 1;
            else if (currentAdress >= 0 && currentAdress <= 1)
                index = currentAdress;
            if (quickInv[index].GetComponent<Item>().itemName == "")
            {
                GameObject.Destroy(quickInv[index]);
                quickInv[index] = item;
                GrapItem(quickInv[index]);
                quickInv[index].GetComponent<Item>().pickUpItem();
                DoSwap(index);
            }
            else
            {
                item.GetComponent<Item>().pickUpItem();
                backpackInv.Add(item);
            }
        }
        else if (item.GetComponent<Item>() is ItemFirstAidKit)
        {
            int index = 3;
            if (quickInv[index].GetComponent<Item>().itemName != "")
            {
                backpackInv.Add(item);
                item.GetComponent<Item>().pickUpItem();
            }
            else
            {
                quickInv[index] = item;
                GrapItem(quickInv[index]);
                quickInv[index].GetComponent<Item>().pickUpItem();
            }
        }
        else if (item.GetComponent<Item>() is ItemAmmo)
        {
            GameObject searchedAmmo = SearchAmmo(item.GetComponent<ItemAmmo>().ammoType);
            if (searchedAmmo != null)
            {
                searchedAmmo.GetComponent<ItemAmmo>().totalNum += item.GetComponent<ItemAmmo>().totalNum;
                Destroy(item);
            }
            else
            {
                item.GetComponent<Item>().pickUpItem();
                backpackInv.Add(item);
            }
        }
        else if (item.GetComponent<Item>() is ItemMelee)
        {
            int index = 1;
            if (quickInv[index].GetComponent<Item>().itemName == "")
            {
                GameObject.Destroy(quickInv[index]);
            }
            else
            {
                quickInv[index].GetComponent<Item>().DropItem_fromPlayer();
            }
            quickInv[index] = item;
            GrapItem(quickInv[index]);
            quickInv[index].GetComponent<Item>().pickUpItem();
            DoSwap(index);
        }
        else if (item.GetComponent<Item>().itemName == "문따개")
        {
            int index = 2;
            if (quickInv[index].GetComponent<Item>().itemName != "")
            {
                backpackInv.Add(item);
                item.GetComponent<Item>().pickUpItem();
            }
            else
            {
                quickInv[index] = item;
                GrapItem(quickInv[index]);
                quickInv[index].GetComponent<Item>().pickUpItem();
            }
        }
        else if (item.GetComponent<Item>().itemName == "마스터 키")
        {
            item.GetComponent<Item>().pickUpItem();
            backpackInv.Add(item);
            SceneManager.LoadScene(4);
        }
        else
        {
            item.GetComponent<Item>().pickUpItem();
            backpackInv.Add(item);
        }
        HUDReport.ReportState("아이템 획득 : " + item.GetComponent<Item>().itemName + "x" + item.GetComponent<Item>().totalNum);
        UIUpdate();
    }
    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(weaponCon.shootPoint.position, weaponCon.shootPoint.transform.forward, out hit, 3, (1 << 5) + (1 << 9) + (1 << 16) + (1 << 10) + (1 << 17) + (1 << 13)))
        {
            if (hit.collider.gameObject.layer == 17 || hit.collider.gameObject.layer == 13)
                return;
            if (hit.collider.gameObject.layer == 5)
                weaponCon.playerCon.m_IsMove = false;
            touchUI.SetActive(true);


            if (Input.GetButtonDown("Fire1"))
            {
                UIUpdate();

                //아이템 박스
                if (hit.collider.GetComponent<ItemBox>() != null)
                {
                    ItemBox box = hit.collider.GetComponent<ItemBox>();
                    for (int i = 0; i < box.BoxInv.Count; i++)
                    {
                        PickUpItemToinv(box.BoxInv[i]);
                    }
                    box.audioSource.PlayOneShot(box.OpenSounds);
                    box.BoxInv.Clear();
                    box.gameObject.layer = 0;
                    box.GetComponent<ReflectionWave>().enabled = false;
                    UIUpdate();
                }
                else if (hit.collider.GetComponent<Item>() != null)
                    PickUpItemToinv(hit.collider.gameObject);
                else if (hit.collider.transform.parent.GetComponent<Door>() != null)
                {
                    Door door = hit.collider.transform.parent.GetComponent<Door>();
                    if (GetCurrentItemInfo().itemName != "문따개")
                    {
                        if (door.lockDoor)
                            door.PlayRandomAudio(door.lockSounds);
                        else
                        {
                            door.anim.SetBool("open", true);
                            door.PlayRandomAudio(door.openSounds);
                        }
                    }
                    if (SearchItem_inBackpack("마스터 키") != null)
                    {
                        door.anim.SetBool("open", true);
                        door.PlayRandomAudio(door.openSounds);
                    }

                }
            }
            if (GetCurrentItemInfo().itemName == "문따개")
            {
                if (Input.GetButton("Fire1"))
                {
                    weaponCon.playerCon.m_IsMove = false;
                    Door door = hit.collider.transform.parent.GetComponent<Door>();
                    if (hit.collider.transform.parent.GetComponent<Door>() != null)
                    {
                        if (!door.anim.GetBool("open"))
                        {
                            if (!door.lockDoor)
                            {
                                door.anim.SetBool("open", true);
                                door.PlayRandomAudio(door.openSounds);
                            }
                            else
                            {
                                GetCurrentItemInfo().PrepareItem();
                                if (!door.audioSource.isPlaying)
                                    door.PlayRandomAudio(door.unlockingSounds);
                                if (GetCurrentItemInfo().IsFullProgressBar())
                                {
                                    door.audioSource.Stop();
                                    door.PlayRandomAudio(door.unlockSounds);
                                    door.lockDoor = false;
                                    GetCurrentItemInfo().totalNum--;
                                    StartCoroutine(GetCurrentItemInfo().StartDestroy());
                                    UIUpdate();
                                    //GetCurrentItemInfo().DoUse();
                                }
                            }
                        }
                    }
                }
                else if (Input.GetButtonUp("Fire1"))
                {
                    Door door = hit.collider.transform.parent.GetComponent<Door>();
                    if (GetCurrentItemInfo().IsProgressingBar())
                        door.audioSource.Stop();
                    GetCurrentItemInfo().CancelUsing();
                }
            }

        }
        else
        {
            if (GetCurrentItemInfo().itemName == "문따개")
                GetCurrentItemInfo().CancelUsing();
            touchUI.SetActive(false);
        }

        /*if (Input.GetKeyDown(KeyCode.R))
            hudStateInfo.UpdateHUDInfo();*/
    }

    public void DropItemFromHand()
    {
        GameObject findItem;
        string findItemName = quickInv[currentAdress].GetComponent<Item>().itemName;
        findItem = SearchItem_inBackpack(findItemName);
        GetCurrentItemInfo().GetComponent<Item>().DropItem_fromPlayer();
        quickInv[currentAdress].transform.SetParent(null);
        if (findItem != null)
        {
            //아이템 리필
            backpackInv.Remove(findItem);
            if (quickInv[currentAdress].GetComponent<Item>().itemName == "")
                Destroy(quickInv[currentAdress]);
            quickInv[currentAdress] = GrapItem(findItem);
        }
        else
        {
            animCon.NoItem();
            quickInv[currentAdress] = Instantiate(packtory.FindItem(""));
            quickInv[currentAdress].transform.SetParent(hand.transform);
        }
    }
    public void DropItem(GameObject item)
    {
        item.GetComponent<Item>().DropItem_fromPlayer();
        item.transform.SetParent(null);
        item = Instantiate(packtory.FindItem(""));
        item.transform.SetParent(hand.transform);
    }
    public void DropItem_fromBackpack(Item item)
    {
        if (backpackInv.Count == 0)
            return;
        int index = 0;
        GameObject itemOBJ = SearchItem_inBackpack(item, out index);

        itemOBJ.GetComponent<Item>().DropItem_fromPlayer();
        itemOBJ.transform.SetParent(null);
        backpackInv.RemoveAt(index);
    }
    public void DrawItem_fromBackpack(Item item)
    {
        if (backpackInv.Count == 0)
            return;
        int index = 0;
        GameObject itemOBJ = SearchItem_inBackpack(item, out index);

        itemOBJ.transform.SetParent(null);
        backpackInv.RemoveAt(index);
    }

    void CheckIndex()
    {
        if (currentAdress > 4)
            currentAdress = 0;
        else if (currentAdress < 0)
            currentAdress = 4;
        if (quickInv[currentAdress].GetComponent<Item>().itemName == "")
            animCon.NoItem();
    }

    public float GetInvWeight()
    {
        weight = 0;
        for (int i = 0; i < 4; i++)
            if (quickInv[i] != null)
                weight += quickInv[i].GetComponent<Item>().i_Weight;
        if (backpackInv.Count > 0)
            for (int i = 0; i < backpackInv.Count; i++)
                if (backpackInv[i] != null)
                    weight += backpackInv[i].GetComponent<Item>().i_Weight;
        return weight;
    }

    public GameObject SearchAmmo(ItemAmmo.AmmoTypes search)
    {
        GameObject find = null;
        if (backpackInv.Count == 0)
            return null;
        for (int i = 0; i < backpackInv.Count; i++)
        {
            if (backpackInv[i] != null && backpackInv[i].GetComponent<Item>() is ItemAmmo && backpackInv[i].GetComponent<ItemAmmo>().ammoType == search)
            {
                find = backpackInv[i];
                break;
            }
        }
        if (find != null)
            return find;
        else
            return null;
    }
    public GameObject SearchItem_inAllInv(Item item)
    {
        GameObject find = null;
        if (quickInv.Length == 0)
            return null;
        for (int i = 0; i < quickInv.Length; i++)
            if (quickInv[i] != null && quickInv[i].GetComponent<Item>().id == item.id)
            {
                find = quickInv[i];
                break;
            }
        if (find == null)
            find = SearchItem_inBackpack(item);
        return find;
    }
    public GameObject SearchItem_inBackpack(string search)
    {
        GameObject find = null;
        if (backpackInv.Count == 0)
            return null;
        for (int i = 0; i < backpackInv.Count; i++)
            if (backpackInv[i] != null && backpackInv[i].GetComponent<Item>().itemName == search)
            {
                find = backpackInv[i];
                break;
            }
        return find;
    }
    public GameObject SearchItem_inBackpack(string search, out int adress)
    {
        GameObject find = null;
        adress = 0;
        if (backpackInv.Count == 0)
            return null;
        for (int i = 0; i < backpackInv.Count; i++)
            if (backpackInv[i] != null && backpackInv[i].GetComponent<Item>().itemName == search)
            {
                find = backpackInv[i];
                adress = i;
                break;
            }
        return find;
    }
    public GameObject SearchItem_inBackpack(Item item)
    {
        GameObject find = null;
        if (backpackInv.Count == 0)
            return null;
        for (int i = 0; i < backpackInv.Count; i++)
            if (backpackInv[i] != null && backpackInv[i].GetComponent<Item>().id == item.id)
            {
                find = backpackInv[i];
                break;
            }
        return find;
    }
    public GameObject SearchItem_inBackpack(Item item, out int adress)
    {
        GameObject find = null;
        adress = 0;
        if (backpackInv.Count == 0)
            return null;
        for (int i = 0; i < backpackInv.Count; i++)
            if (backpackInv[i] != null && backpackInv[i].GetComponent<Item>().id == item.id)
            {
                find = backpackInv[i];
                adress = i;
                break;
            }
        return find;
    }
    public int CountItem_inBackpack(string search)
    {
        int find = 0;
        if (backpackInv.Count == 0)
            return 0;
        for (int i = 0; i < backpackInv.Count; i++)
            if (backpackInv[i] != null && backpackInv[i].GetComponent<Item>().itemName == search)
                find++;
        for (int i = 0; i < quickInv.Length; i++)
            if (quickInv[i] != null && quickInv[i].GetComponent<Item>().itemName == search)
                find++;
        return find;
    }
    void QuickInvInitialize()
    {
        for (int i = 0; i < 5; i++)
            if (quickInv[i] != null)
            {
                quickInv[i].transform.SetParent(hand.transform);
                quickInv[i].transform.localPosition = quickInv[i].GetComponent<Item>().HandPosition;
                quickInv[i].transform.localRotation = Quaternion.Euler(quickInv[i].GetComponent<Item>().HandRotation.x, quickInv[i].GetComponent<Item>().HandRotation.y, quickInv[i].GetComponent<Item>().HandRotation.z);
                if (i != 0)
                    quickInv[i].SetActive(false);
            }
    }
    public void UIUpdate()
    {
        if (GetCurrentItemInfo() is ItemGun)
            weaponCon.gun = (ItemGun)GetCurrentItemInfo();
        for (int i = 0; i < 5; i++)
        {
            if (quickInv[currentAdress] != null)
            {
                if (quickInv[i] != null)
                {
                    if (quickInv[i].GetComponent<Item>().totalNum <= 0)
                    {
                        GameObject findItem;
                        string findItemName = quickInv[i].GetComponent<Item>().itemName;
                        findItem = SearchItem_inBackpack(findItemName);

                        if (findItem != null)
                        {
                            //아이템 리필
                            backpackInv.Remove(findItem);
                            quickInv[i] = GrapItem(findItem);
                        }
                        else
                        {
                            GameObject handItem = Instantiate(packtory.FindItem(""));
                            quickInv[i].transform.SetParent(null);
                            quickInv[i] = GrapItem(handItem);
                        }
                    }
                    else
                    {
                        if (quickInv[i].GetComponent<Item>().id == quickInv[currentAdress].GetComponent<Item>().id)
                            quickInv[i].SetActive(true);
                        else
                            quickInv[i].SetActive(false);
                    }
                }
            }
            else if (hand.transform.childCount >= i + 1 && hand.transform.GetChild(i) != null)
                hand.transform.GetChild(i).gameObject.SetActive(false);
        }
        hudStateInfo.UpdateHUDInfo();
        animCon.NoItem();
        animCon.FindHandPosition();
        uiInv.UpdateUI();
    }

    public Item GetCurrentItemInfo()
    {
        if (quickInv[currentAdress] != null)
        {
            Item item = quickInv[currentAdress].GetComponent<Item>();
            return item;
        }
        return null;
    }

    public void SwitchItem(float i)
    {
        if (i < 0)
            wheelIndex++;
        else
            wheelIndex--;
        if (wheelIndex > 4)
            wheelIndex = 0;
        else if (wheelIndex < 0)
            wheelIndex = 4;
        DoSwap(wheelIndex);
        /*checkIndex();
        UIInvUpdate();
        getInvWeight();*/
    }
    public void DoSwap(int i)
    {
        if (currentAdress == i && i == 0)
            weaponCon.DoReload();
        if (currentAdress == i || nextIndex == i || weaponCon.isMeleeAttacking)
            return;
        if (GetCurrentItemInfo() is ItemMelee && !((ItemMelee)GetCurrentItemInfo()).canSwing) return;
        GetCurrentItemInfo().CancelUsing();

        if (hudProgressBar != null)
            hudProgressBar.imageProgressBar.fillAmount = 0;
        isSwaping = true;
        nextIndex = i;
        previousIndex = currentAdress;
        if (nextIndex > 4)
            nextIndex = 0;
        else if (nextIndex < 0)
            nextIndex = 4;
        wheelIndex = nextIndex;
        anim.CrossFadeInFixedTime("PutIn", 0.01f);
    }
    public void ChangeCurrentAdress(int i)
    {
        currentAdress = i;
        CheckIndex();
        weaponCon.isAiming = false;
        hand.transform.parent.GetComponent<WeaponSway>().initialIPositing = weaponCon.originPos;
        weaponCon.maincam.fieldOfView = 60;
        UIUpdate();
    }
    public GameObject GrapItem(GameObject item)
    {
        item.transform.SetParent(hand.transform);
        item.transform.localPosition = item.GetComponent<Item>().HandPosition;
        item.transform.localRotation = Quaternion.Euler(item.GetComponent<Item>().HandRotation.x, item.GetComponent<Item>().HandRotation.y, item.GetComponent<Item>().HandRotation.z);
        return item;
    }
}
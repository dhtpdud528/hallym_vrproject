﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class PlayerAnimController : MonoBehaviour
{
    public Animator anim;
    private CharacterController m_CharacterController;
    private WeaponController weaponCon;
    private Transform rh;
    private Transform lh;
    public Transform noRH;
    public Transform noLH;
    private PlayerInv inv;
    // Start is called before the first frame update
    void Awake()
    {
        anim = GetComponent<Animator>();
        weaponCon = FindObjectOfType<WeaponController>();
        m_CharacterController = FindObjectOfType<VRSoundFirstPersonController>().GetComponent<CharacterController>();
        inv = FindObjectOfType<PlayerInv>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (weaponCon.playerCon.cantMove)
            anim.SetFloat("Speed", 0);
        else
            anim.SetFloat("Speed", m_CharacterController.velocity.magnitude / 5);
        anim.SetFloat("Horizontal", Input.GetAxis("Horizontal") * m_CharacterController.velocity.magnitude / 5);
        anim.SetFloat("Vertical", Input.GetAxis("Vertical") * m_CharacterController.velocity.magnitude / 5);
    }
    void OnAnimatorIK(int layerIndex)
    {
        if (rh != null)
        {
            anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 1);
            anim.SetIKRotationWeight(AvatarIKGoal.RightHand, 1);
            anim.SetIKPosition(AvatarIKGoal.RightHand, rh.position);
            anim.SetIKRotation(AvatarIKGoal.RightHand, rh.rotation);
        }
        else
        {
            anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 1);
            anim.SetIKRotationWeight(AvatarIKGoal.RightHand, 1);
            anim.SetIKPosition(AvatarIKGoal.RightHand, noRH.position);
            anim.SetIKRotation(AvatarIKGoal.RightHand, noRH.rotation);
        }
        if (lh != null)
        {
            anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1);
            anim.SetIKRotationWeight(AvatarIKGoal.LeftHand, 1);
            anim.SetIKPosition(AvatarIKGoal.LeftHand, lh.position);
            anim.SetIKRotation(AvatarIKGoal.LeftHand, lh.rotation);
        }
        else
        {
            anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1);
            anim.SetIKRotationWeight(AvatarIKGoal.LeftHand, 1);
            anim.SetIKPosition(AvatarIKGoal.LeftHand, noLH.position);
            anim.SetIKRotation(AvatarIKGoal.LeftHand, noLH.rotation);
        }
    }
    public void FindHandPosition()
    {
        Transform item = inv.GetCurrentItemInfo().transform;
        for (int i = 0; i < item.childCount; i++)
        {
            if (item.GetChild(i).name == "RH")
                rh = item.GetChild(i);
            else if (item.GetChild(i).name == "LH")
                lh = item.GetChild(i);
        }

    }
    public void NoItem()
    {
        rh = null;
        lh = null;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class PlayerHandAnimController : MonoBehaviour
{
    private Animator anim;
    private VRSoundFirstPersonController playerCon;
    private WeaponController weaponCon;
    // Start is called before the first frame update
    void Awake()
    {
        anim = GetComponent<Animator>();
        playerCon = FindObjectOfType<VRSoundFirstPersonController>();
        weaponCon = FindObjectOfType<WeaponController>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(!playerCon.m_IsWalking && !weaponCon.isReloading && !weaponCon.isAiming&&playerCon.GetComponent<CharacterController>().velocity.magnitude > 0.1f)
            anim.SetBool("Run",true);
        else
            anim.SetBool("Run", false);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PutInState : StateMachineBehaviour
{
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetFloat("DrawSpeed", animator.GetComponent<PlayerInv>().GetCurrentItemInfo().drawSpeed);
    }
    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (stateInfo.normalizedTime >= 0.9)
            animator.GetComponent<PlayerInv>().ChangeCurrentAdress(animator.GetComponent<PlayerInv>().nextIndex);
    }
}

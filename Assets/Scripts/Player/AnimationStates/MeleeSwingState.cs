﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeSwingState : StateMachineBehaviour
{
    private WeaponController weaponCon;
    private PlayerInv playerInv;
    private void Awake()
    {
        weaponCon = FindObjectOfType<WeaponController>();
        playerInv = FindObjectOfType<PlayerInv>();
    }
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (playerInv.GetCurrentItemInfo() is ItemMelee)
            ((ItemMelee)playerInv.GetCurrentItemInfo()).canSwing = false;
        weaponCon.shootPoint.GetComponent<MeleeHitBox>().maxStack = playerInv.GetCurrentItemInfo().maxHit;
    }
    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (0.1f <= stateInfo.normalizedTime && stateInfo.normalizedTime <= 0.7f)
        {
            weaponCon.shootPoint.GetComponent<MeleeHitBox>().damage =
                playerInv.GetCurrentItemInfo() is ItemMelee ?
                weaponCon.isMeleeAttacking ? 1 : ((ItemMelee)playerInv.GetCurrentItemInfo()).damege :
                1;
            if (playerInv.GetCurrentItemInfo() is ItemMelee)
                if (((ItemMelee)playerInv.GetCurrentItemInfo()).damege > ((ItemMelee)playerInv.GetCurrentItemInfo()).damegeOrigin)//차지상태일때
                {
                    if (((ItemMelee)playerInv.GetCurrentItemInfo()).chargedMulti)//멀티타격
                        weaponCon.shootPoint.GetComponents<BoxCollider>()[1].enabled = true;
                    else
                        weaponCon.shootPoint.GetComponents<BoxCollider>()[0].enabled = true;
                }
                else//차지상태아닐때
                {
                    if (((ItemMelee)playerInv.GetCurrentItemInfo()).quickMulti)//멀티타격
                        weaponCon.shootPoint.GetComponents<BoxCollider>()[1].enabled = true;
                    else
                        weaponCon.shootPoint.GetComponents<BoxCollider>()[0].enabled = true;
                }
            else
                weaponCon.shootPoint.GetComponents<BoxCollider>()[1].enabled = true;

        }
        else
        {
            for (int i = 0; i < weaponCon.shootPoint.GetComponents<BoxCollider>().Length; i++)
                weaponCon.shootPoint.GetComponents<BoxCollider>()[i].enabled = false;
            weaponCon.shootPoint.GetComponent<MeleeHitBox>().ids.Clear();
            weaponCon.shootPoint.GetComponent<MeleeHitBox>().currentStack = 0;
        }
        if (playerInv.GetCurrentItemInfo() is ItemMelee)
            if (stateInfo.normalizedTime >= 0.9)
                ((ItemMelee)playerInv.GetCurrentItemInfo()).canSwing = true;
    }
}

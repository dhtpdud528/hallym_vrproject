﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;
using UnityEngine.SceneManagement;

public class PlayerState : MonoBehaviour
{
    public WeaponController weaponCon;
    public PlayerInv playerInv;
    public float maxHeath = 100;
    private float health;

    public float maxStemina = 100;
    public float stemina;

    public float maxLungs = 20;
    public float lungs;
    public enum m_deburf { Down = 0, Bloody = 1, Confusion = 2, Slow = 3 };
    public bool down = false;
    public bool bloody = false;
    public bool confusion = false;
    public bool slow = false;
    public float confusionTimeMax = 4;
    [SerializeField] private float confusionTime;
    private float confusionValue = 0;

    [SerializeField]private float relax;
    public float maxDownTime;
    [SerializeField] private float downTime = 0;

    float playerHeight_Origin;
    Vector3 playerCenter_Origin;
    float playerWalkSpeed_Origin;
    float playerRunSpeed_Origin;
    VRSoundFirstPersonController playerCon;
    private HUDProgressBar hudProgressBar;

    /*private PostProcessingBehaviour postProcessing;
    [SerializeField] private MotionBlurModel.Settings settings;*/
    private HUDStateInfo hudQuickInfo;
    private float playerCurrentCenter;
    public bool standUp;
    public GameObject maincam;
    [SerializeField] private float slowTime;
    public float slowMaxTime;

    private float walkSpeedOrigin;
    private float runSpeedOrigin;

    public AudioSource audioSource;
    public AudioClip[] punchSounds;
    public AudioClip[] stompSounds;
    public AudioClip[] gruntsSounds;
    public AudioClip[] criticalSounds;
    public AudioClip[] bloodSounds;
    public bool isPain;
    MicroponeSoundWaveSpawner micropone;

    // Use this for initialization
    void Start()
    {
        micropone = FindObjectOfType<MicroponeSoundWaveSpawner>();
        audioSource = GetComponent<AudioSource>();
        relax = 100;
        slowTime = 0;
        weaponCon = FindObjectOfType<WeaponController>();
        playerInv = FindObjectOfType<PlayerInv>();
        maincam = Camera.main.gameObject;
        standUp = true;
        hudProgressBar = FindObjectOfType<HUDProgressBar>();
        health = maxHeath;
        confusionTime = confusionTimeMax;
        hudQuickInfo = FindObjectOfType<HUDStateInfo>();

        playerHeight_Origin = GetComponent<CharacterController>().height;
        playerCenter_Origin = GetComponent<CharacterController>().center;
        playerCon = GetComponent<VRSoundFirstPersonController>();
        playerWalkSpeed_Origin = playerCon.m_WalkSpeed;
        playerRunSpeed_Origin = playerCon.m_RunSpeed;
        /*postProcessing = FindObjectOfType<PostProcessingBehaviour>();
        settings.shutterAngle = 360;
        settings.sampleCount = 10;
        settings.frameBlending = 0;
        postProcessing.profile.motionBlur.settings = settings;*/
        walkSpeedOrigin = playerCon.m_WalkSpeed;
        runSpeedOrigin = playerCon.m_RunSpeed;
    }

    public void AddRelax(float i)
    {
        if (relax + i < 0)
            relax = 0;
        else
            relax += i;
    }
    public float GetRelax()
    {
        return relax;
    }
    // Update is called once per frame
    void Update()
    {
        lungs += Time.deltaTime;
        lungs -= micropone.audioPeer.volume*20;
        if (lungs > maxLungs)
            lungs = maxLungs;
        else if (lungs < 0)
            lungs = 0;
        if (lungs >= maxLungs)
            TakeDamage(Time.deltaTime*10);
        if (bloody && health > maxHeath / 200)
            TakeDamage(maxHeath / 200 * Time.deltaTime);
        if (playerCon.isSit)
        {
            if (GetComponent<CharacterController>().height > 0)
                GetComponent<CharacterController>().height -= Time.deltaTime * 2;
            if (GetComponent<CharacterController>().center.y < 0.5f)
            {
                playerCurrentCenter += Time.deltaTime * 2;
                if (playerCurrentCenter > 0.5f)
                    playerCurrentCenter = 0.5f;
                GetComponent<CharacterController>().center = new Vector3(0, playerCurrentCenter, 0);
            }
        }
        else if (!down)
        {
            playerCon.m_RunSpeed = runSpeedOrigin / (slow ? 4 : 1);
            playerCon.m_WalkSpeed = walkSpeedOrigin / (slow ? 4 : 1);
            if (GetComponent<CharacterController>().height < playerHeight_Origin)
            {
                standUp = false;
                GetComponent<CharacterController>().height += Time.deltaTime * 2;
                if (GetComponent<CharacterController>().height > playerHeight_Origin)
                {
                    standUp = true;
                    GetComponent<CharacterController>().height = playerHeight_Origin;
                }
            }
            if (GetComponent<CharacterController>().center.y > playerCenter_Origin.y)
            {
                playerCurrentCenter -= Time.deltaTime * 2;
                if (playerCurrentCenter < playerCenter_Origin.y)
                    playerCurrentCenter = playerCenter_Origin.y;
                GetComponent<CharacterController>().center = new Vector3(0, playerCurrentCenter, 0);
            }
        }
        if (confusion)
        {
            if (confusionTime == confusionTimeMax)
            {
                confusionValue = 1;
                /*settings.frameBlending = confusionValue;
                postProcessing.profile.motionBlur.settings = settings;*/
            }
            if (confusionTime > 0)
                confusionTime -= Time.deltaTime;
            else
            {
                if (confusionValue > 0)
                {
                    confusionValue -= Time.deltaTime;
                    /*settings.frameBlending = confusionValue;
                    postProcessing.profile.motionBlur.settings = settings;*/
                }
                else
                {
                    confusionTime = confusionTimeMax;
                    confusionValue = 0;
                    confusion = false;
                }
            }
        }
        if (slow)
        {
            if (slowTime < slowMaxTime)
                slowTime += Time.deltaTime;
            else
            {
                slowTime = 0;
                slow = false;
            }
        }
        if (down)
        {
            if (Input.GetKey(KeyCode.X))
            {
                downTime += Time.deltaTime;
                if (hudProgressBar.imageProgressBar.fillAmount < 1)
                    hudProgressBar.imageProgressBar.fillAmount += Time.deltaTime / maxDownTime;
                playerCon.m_WalkSpeed = 0;
                playerCon.m_RunSpeed = 0;
            }
            else
            {
                hudProgressBar.imageProgressBar.fillAmount = 0;
                downTime = 0;
                GetComponent<CharacterController>().height = 0;
                GetComponent<CharacterController>().center = new Vector3(0, 1, 0);
                playerCon.m_WalkSpeed = 1;
                playerCon.m_RunSpeed = 1;
            }
            if (downTime >= maxDownTime)
            {
                hudProgressBar.imageProgressBar.fillAmount = 0;
                GetComponent<CharacterController>().height = playerHeight_Origin;
                GetComponent<CharacterController>().center = playerCenter_Origin;
                transform.position = new Vector3(transform.position.x, transform.position.y + GetComponent<CharacterController>().height, transform.position.z);
                playerCon.m_WalkSpeed = playerWalkSpeed_Origin;
                playerCon.m_RunSpeed = playerRunSpeed_Origin;
                downTime = 0;
                if (health <= 0)
                    health = 10;
                down = false;
            }
        }
        if (health > maxHeath)
            health = maxHeath;
        else if (health <= 0)
        {
            health = 0;
            AI_Witch ai8feet = FindObjectOfType<AI_Witch>();
            if (ai8feet == null ||
                Vector3.Distance(transform.position, ai8feet.transform.position) > 2f)
                down = true;
        }

        if (stemina > maxStemina)
            stemina = maxStemina;
        else if (stemina < 0)
            stemina = 0;

        if (relax > 100)
            relax = 100;
        else if (relax < 0)
            relax = 0;
    }
    public void TakeDamage(float damage, m_deburf deburf)
    {
        TakeDamage(damage);
        switch (deburf)
        {
            case m_deburf.Bloody:
                bloody = true;
                break;
            case m_deburf.Confusion:
                confusion = true;
                break;
            case m_deburf.Down:
                down = true;
                break;
            case m_deburf.Slow:
                slowTime = 0;
                slow = true;
                break;
        }
        hudQuickInfo.TakeDamage_HUD(damage * 5);
    }
    public void TakeDamage(float damage)
    {
        //maincam.GetComponent<CameraShaker>().ShakeOnce(damage * 2, damage * 2, 0, 0.5f);

        if (damage > 0)
            health -= damage;
            
        if (!bloody)
            hudQuickInfo.TakeDamage_HUD(damage * 10);
        if (damage >= 15)
        {
            audioSource.PlayOneShot(stompSounds[Random.Range(0, stompSounds.Length)]);
            if (!down)
                if (health / maxHeath < 0.5f)
                    audioSource.PlayOneShot(criticalSounds[Random.Range(0, criticalSounds.Length)]);
                else
                    audioSource.PlayOneShot(gruntsSounds[Random.Range(0, gruntsSounds.Length)]);
            else
                audioSource.PlayOneShot(bloodSounds[Random.Range(0, bloodSounds.Length)]);
            StartCoroutine(playerPain());
        }
        else if(damage >= 5)
            if (playerInv.GetCurrentItemInfo() is ItemFirstAidKit)
                hudProgressBar.imageProgressBar.fillAmount = 0;
            else if(damage >= 1)
            audioSource.PlayOneShot(punchSounds[Random.Range(0, punchSounds.Length)]);

        if(health <= 0)
            StartCoroutine(Dead());


    }
    IEnumerator Dead()
    {
        down = true;
        yield return new WaitForSeconds(5);
        SceneManager.LoadScene(3);
    }
    public double GetHealth()
    {
        return Math.Truncate(health * 10) / 10;
    }
    public void AddHealth(float i)
    {
        health += i;
    }
    IEnumerator playerPain()
    {
        isPain = true;
        yield return new WaitForSeconds(1);
        isPain = false;
        StopAllCoroutines();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerRigidbody : MonoBehaviour {
    float mass;
    Vector3 impact = Vector3.zero;
    private CharacterController character;

	// Use this for initialization
	void Start () {
        character = GetComponent<CharacterController>();
        //mass = GetComponent<Rigidbody>().mass;
    }
	
	// Update is called once per frame
	void Update () {
        if (impact.magnitude > 0.2)
            character.Move(impact * Time.deltaTime);
        impact = Vector3.Lerp(impact, Vector3.zero, 5 * Time.deltaTime);
	}
    public void addImpact(Vector3 dir, float force)
    {
        dir.Normalize();
        if (dir.y < 0) dir.y = -dir.y;
        impact += dir.normalized * force/mass;
    }
    public void addAcceleration(Vector3 dir, float force)
    {
        dir.Normalize();
        if (dir.y < 0) dir.y = -dir.y;
        impact += dir.normalized * force;
    }
}

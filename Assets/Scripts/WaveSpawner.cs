﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[RequireComponent(typeof(AudioPeer))]
public class WaveSpawner : MonoBehaviour
{
    [SerializeField] GameObject wave;
    Transform wavePoket;
    public float sensitivity;
    public float maxVolume;
    AudioPeer audioPeer;
    AudioSource audioSource;

    public bool isVirtualSound;
    // Start is called before the first frame update
    private void Awake()
    {
        audioPeer = GetComponent<AudioPeer>();
        
        wavePoket = GameObject.Find("Waves").transform;
    }
    void Start()
    {
        audioSource = audioPeer._audioSource;
    }
    private void OnEnable()
    {
        StartCoroutine(spawnWaves());
    }
    private void OnDisable()
    {
        StopAllCoroutines();
    }

    // Update is called once per frame
    void Update()
    {
        if(isVirtualSound && !audioSource.isPlaying)
            isVirtualSound = false;
    }
    IEnumerator spawnWaves()
    {
        while (true)
        {
            yield return new WaitForSeconds(.05f);
            if (audioPeer.volume > 0)
            {
                SoundWave spawnedWave = Instantiate(wave, transform.position, new Quaternion()).GetComponent<SoundWave>();
                spawnedWave.volume = audioPeer.volume * sensitivity < maxVolume ? audioPeer.volume * sensitivity : maxVolume;
                spawnedWave.transform.SetParent(wavePoket);
                spawnedWave.owner = gameObject;
                spawnedWave.isVirtualSound = isVirtualSound;
            }
        }
    }
}
